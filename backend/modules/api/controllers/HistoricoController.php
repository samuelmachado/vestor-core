<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Historico;
use common\models\CondutorRota;
use common\models\Aluno;
use common\models\HistoricoAluno;
use common\models\Usuario;
use common\models\HistoricoEscola;
use common\models\Escola;

class HistoricoController extends ActiveController
{
    public $modelClass = 'common\models\Historico';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
    public function actionCreate(){
          $dados = json_decode(file_get_contents('php://input'), true);
          $historico = new Historico();
          $historico->idCondutor = $dados['idCondutor'];
          $historico->idVeiculo = $dados['idVeiculo'];
          $historico->idCondutorRota = $dados['idCondutorRota'];
          $historico->data = $dados['data'];
          $historico->checkIn = $dados['checkIn'];
          $historico->checkOut = $dados['checkOut'];
          $rota = CondutorRota::findOne($dados['idCondutorRota']);


          if(!$historico->save()){
            return ['status' => false, 'message' => 'Não foi possível salvar o histórico', 'errors' => $historico->getErrors()];
          }

          if(isset($dados['escolas'])) {
            $escolasPontos = [];
            foreach ($rota->escolaPonto as $escola) {
                  $escolasPontos[$escola->idEscola] = ['lat' => $escola->ponto->lat, 'lng' => $escola->ponto->lng];
            }
            foreach ($dados['escolas'] as $escola) {
                $e = new HistoricoEscola();
                $e->idHistorico = $historico->id;
                $e->idEscola = $escola['idEscola'];
                $e->checkOut = $escola['checkOut'];
                if(isset($escolasPontos[$escola['idEscola']])){
                    $e->lat = $escolasPontos[$escola['idEscola']]['lat'];
                    $e->lng = $escolasPontos[$escola['idEscola']]['lng'];
                }
                $e->save();
            }
          }

          if(isset($dados['alunos'])) {
            $alunosPontos = [];
            foreach ($rota->alunoPonto as $aluno) {
                  $alunosPontos[$aluno->idAluno] = ['lat' => $aluno->ponto->lat, 'lng' => $aluno->ponto->lng];
            }

            foreach ($dados['alunos'] as $aluno) {
                $a = new HistoricoAluno();
                $a->idHistorico = $historico->id;
                $a->idAluno = $aluno['idAluno'];
                $a->checkIn  = $aluno['checkIn'];
                $a->checkOut = $aluno['checkOut'];
                if(isset($alunosPontos[$aluno['idAluno']])){
                    $a->lat = $alunosPontos[$aluno['idAluno']]['lat'];
                    $a->lng = $alunosPontos[$aluno['idAluno']]['lng'];
                }
                $a->save();
            }
          }

        return ['status' => true];
    }
    public function actionCondutor($idCondutor){
        $idsRotas = CondutorRota::find()
                                    ->select('id')
                                    ->where(['idCondutor' => $idCondutor])
                                    ->asArray()
                                    ->all();

        //Transforma key => value em uma lista simples
        $idsRotas = array_column($idsRotas, 'id');
        return Historico::find()->where(['in', 'idCondutorRota', $idsRotas])->all();
    }

    public function actionResponsavel($idUsuario){
        $usuario = Usuario::findOne($idUsuario);

        if($usuario->cpf){
            $filhos = Aluno::find()
                                ->where(['REPLACE(REPLACE(cpfResponsavel, ".",""),"-","")' => $usuario->cpf])
                                ->all(); 
           

            // $historicos = HistoricoAluno::find()->where(['in', 'idAluno', $idsFilhos])->all();   
            
            // $output = [];
            // foreach ($historicos as $historico) {
            //     $output[$historico->idAluno] = $historico ;
            // }
                  
            // foreach ($output as $out) {
            //     $res[] = $out;
            // }

        // foreach ($historicos as $historico) { 
        //     $start =  $historico->idAluno;
        //     if(!in_array($start, $output)){
        //          array_push($output, $start);
        //     } 
        // } 

        // $response = [];

        // foreach ($output as $out) {
        //     $reservasDoDia = [];
        //     foreach ($historicos as $historico) {
        //         if($historico->idAluno == $out){
        //             array_push( $reservasDoDia, $historico);
        //         }
        //     }
        //    array_push($response, [$out, $reservasDoDia]);
        // }

            //if($histoicos){
                return ['status' => true, 'data' => $filhos, 'message' => 'OK!'];
            //}

        }
        return ['status' => false, 'message' => 'CPF inexistente/inválido'];
        
    }
}   