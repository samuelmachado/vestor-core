<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\CondutorRota;
use common\models\Ponto;
use common\models\PontoAluno;
use common\models\PontoEscola;
use common\models\SolicitacaoTransporte;
use common\models\Usuario;
use common\models\Aluno;

class RotaController extends ActiveController
{
    public $modelClass = 'common\models\CondutorRota';
    // 70
    // public function init()
    // {
    //     // $id = \Yii::$app->User->identity->id;
    //     \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //     parent::init();
    // }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
         unset($actions['index']);
        // unset($actions['update']);
         unset($actions['create']);
        return $actions;
    }
    

    public function actionIndex($idCondutor){
        return CondutorRota::find()->where(['=','idCondutor',$idCondutor])->all();
    }

    public function actionRoterizar($idCondutorRota){
        $model = CondutorRota::findOne($idCondutorRota);
        return $this->render('roterizar', [
            'model' => $model,
        ]);
    }

    public function actionCreate(){
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
         $rotas = json_decode(file_get_contents('php://input'), true);
         if($rotas) {
             foreach ($rotas as $rota) {
                 $condutorRota = new CondutorRota();
                 $condutorRota->idCondutor = null;
                    if(isset($rota['idCondutor']) && $rota['idCondutor'] != ''){

                        $condutorRota->idCondutor = $rota['idCondutor'];
                    }
                    
                 $condutorRota->descricao  = $rota['nome'];
                 $condutorRota->sentido = $rota['sentido'];
                 $condutorRota->save();
            

                 if($rota['percurso']){
                    foreach ($rota['percurso'] as $pontoPost) {
                        //print 'entrou em percurso';
                        $ponto = new Ponto();

                        switch ($pontoPost['statusCode']) {
                            case Ponto::PONTO_INICIO : $ponto->tipo = Ponto::PONTO_INICIO;  break;
                            case Ponto::PONTO_NADA : $ponto->tipo = Ponto::PONTO_NADA;  break;
                            case Ponto::PONTO_ESCOLA : $ponto->tipo = Ponto::PONTO_ESCOLA;  break;
                            case Ponto::PONTO_ALUNO : $ponto->tipo = Ponto::PONTO_ALUNO;  break;
                            case Ponto::PONTO_ENCONTRO : $ponto->tipo = Ponto::PONTO_ENCONTRO;  break;
                         }    
                            $ponto->idCondutorRota = $condutorRota->id;

                            $ponto->lat = $pontoPost['latitude'];
                            $ponto->lng = $pontoPost['longitude'];

                           $ponto->save();
                               

                            if(isset($pontoPost['alunos'])){
                                //print 'entrou em entrou em alunos';
                                if($pontoPost['alunos'])
                                    foreach ($pontoPost['alunos'] as $aluno) {
                                        SolicitacaoTransporte::retornarAtendido($aluno['id']);

                                        $pontoAluno = new PontoAluno();
                                        $pontoAluno->idPonto = $ponto->id;
                                        $pontoAluno->idAluno = $aluno['id'];
                                        $pontoAluno->save();
                                    }
                            }

                            if(isset($pontoPost['escolas'])){
                                if($pontoPost['escolas'])
                                    foreach ($pontoPost['escolas'] as $escola) {
                                        if(isset($escola['id'])){
                                            $pontoAluno = new PontoEscola();
                                            $pontoAluno->idPonto = $ponto->id;
                                            $pontoAluno->idEscola = $escola['id'];
                                            $pontoAluno->save();
                                        }
                                    }
                            }  
                    }
                 }
             }
         }
         return ['status' => true, 'post' => $rotas];
    }


   public function actionAoVivo($idUsuario){
        $usuario = Usuario::findOne($idUsuario);
        $output = [];
        if($usuario->cpf){
            $filhos = Aluno::find()
                                ->where(['REPLACE(REPLACE(cpfResponsavel, ".",""),"-","")' => $usuario->cpf])
                                ->all(); 

      
        if($filhos){
            foreach ($filhos as $filho) {
                if($filho->alunoPonto){
                     $output[] = [
                            'nome' => $filho->nome,
                            'condutor' => $filho->alunoPonto->ponto->condutor->nome,
                            'veiculo' => $filho->alunoPonto->ponto->condutor->veiculo
                        ];
                }
               
            }
        }
        return ['status' => true, 'message' => 'OK', 'data' => $output];
    } 
        return ['status' => false, 'message' => 'Nenhum aluno associado ao responsável.'];
   
 
  }
}  