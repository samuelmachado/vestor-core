<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Ocorrencia;

class OcorrenciaController extends ActiveController
{
    public $modelClass = 'common\models\Ocorrencia';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        //unset($actions['create']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
    public function actionCondutor($idCondutor){
        return Ocorrencia::find()->where(['idCondutor' => $idCondutor])->all();
    }
   
}   