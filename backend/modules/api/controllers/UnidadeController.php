<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Unidade;

class UnidadeController extends ActiveController
{
    public $modelClass = 'common\models\Unidade';

    public function init()
    {
        // $id = \Yii::$app->User->identity->id;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        parent::init();

    }

	public function behaviors()
	{
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	    	'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
	}

    public function actions()
    {
        $actions = parent::actions();
         //unset($actions['index']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
    public function actionOrigens(){
        $unidades = Unidade::find()->all();
        $output = [];
    
        foreach ($unidades as $unidade) {
            if($unidade->veiculos)
            array_push($output, $unidade);
        }

        return $output;
    }
 
  
}  