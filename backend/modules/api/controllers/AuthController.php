<?php

namespace backend\modules\api\controllers;

use yii\rest\ActiveController;
use frontend\models\PasswordResetRequestForm;

/**
 * Default controller for the `api` module
 */
class AuthController extends ActiveController
{
    public $modelClass = 'common\models\Usuario';
    // protected function verbs()
    // {
    //     return [
    //         'login' => ['POST'],
    //     ];
    // }
    public function actionLogin(){
        // $_POST = json_decode(file_get_contents('php://input'), true);
        
        $username = $_POST['usuario'];
        $password = $_POST['senha'];

        $response = [];
    
        if(!isset($username) || !isset($username)){
          return    $response = [
            'status' => false,
            'message' => 'Usuário e senha precisam ser preenchidos.',
            'data' => '',
          ];
        }
            
            $user = \common\models\Usuario::findByUsername(trim($username));
            
            if(!empty($user)){
             
              if($user->validatePassword($password)){
                if (!$user->authKey)
                {
                  $user->generateAuthKey(); 
                  $user->save();
                } 
                
                return $response = [
                  'status' => true,
                  'message' => 'login OK!',
                  'data' => $user,
                ];
              } else {
                return $response = [
                  'status' => false,
                  'message' => 'Senha inválida',
                  'data' => '',
                ];
              }
              
            } else {
              return $response = [
                'status' => false,
                'message' => 'Usuário não encontrado no banco de dados',
                'data' => print_r($_POST['usuario'], true),
              ];
            }
       
       return 'OK';
     
       
    }


    // public function actionSignup()
    // {
    //     //$_POST = json_decode(file_get_contents('php://input'), true);

    //     $nome = $_POST['nome'];
    //     $email = $_POST['usuario'];
    //     $password = $_POST['senha'];
    //     $idPerfil = $_POST['idPerfil'];
    //     $idFirebase = $_POST['idFirebase'];

    //     $response = [];
        
    //     if(!$nome || !$password || !$email || !$idPerfil || !$idFirebase) {
    //       $response = [
    //         'status' => false,
    //         'message' => 'Informações incompletas.',
    //         'data' => $_POST,
    //       ];
    //     }
    //     else
    //     {
    //         $user = new \common\models\Usuario();
    //         $user->nome = $nome;
    //         $user->email = $email;
    //         $user->idFirebase = $idFirebase;
    //         $user->idPerfil = $idPerfil;

    //         $user->setPassword($password);
    //         $user->generateAuthKey();
    //         if($user->save()){

    //           $response = [
    //             'status' => true,
    //             'message' => 'Cadastrado com sucesso.',
    //             'data' => $user,
    //           ];
    //         } else {
    //             $response = [
    //             'status' => false, 
    //             'message' => 'Este email já está sendo utilizado',
    //             'data' => $user->errors,
    //           ];
    //         }
         
    //     }

    //     return $response;
    // }

    public function actionRecover()
      {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $email = $_POST['email'];
        $user = \common\models\Usuario::findByUsername($email);

        if(!$user) {
          return $response = [
              'status' => false,
               'message' => 'Usuário não encontrado no banco de dados',
              'data' => [
                'id' => '',
              ]
            ];
          }

          $model = new PasswordResetRequestForm();
          $model->email = $email;
          if($model->validate()){
              $mail = $model->sendEmail();
              
              $response = [
                'status' => true,
                'message' => 'Senha enviada',
                'data' => $mail
              ];
              
            
          } else {
              $response = [
                'status' => false,
                'message' => 'Erro ao enviar recuperação de senha',
                'data' => [
                  'id' => $model->getErrors(),
                ]
              ];
          } 
        
          return $response;
      }

    // public function actionSetRegistrationId ($id)
    // {
    //   $_PUT = json_decode(file_get_contents('php://input'), true);
    //   $model = \common\models\Usuario::findOne($id); 

    //   if ($model)
    //   {
    //     $model->idFirebase = $_PUT['idFirebase'];
    //     if ($model->save())
    //       return ['status' => 1, 'message' => 'Usuário salvo com sucesso', 'data' => $_PUT['idFirebase']];
    //     else
    //       return ['status' => 0, 'message' => 'Erro ao salvar usuário', 'data' => $model->getErrors()];
    //   }
    //   else
    //     return ['status' => -1, 'message' => 'Usuário inexistente'];
    // }


    // public function actionTestEmail(){
    //     $model = new PasswordResetRequestForm();
    //     $model->email = 'samuel.codigo@gmail.com';
    //     $mail = $model->sendEmail();
    //     return $mail;
    //     //print_r($model->getErrors());

    // }
}
