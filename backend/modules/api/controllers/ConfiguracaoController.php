<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Configuracao;

class ConfiguracaoController extends ActiveController
{
    public $modelClass = 'common\models\Configuracao';

    public function init()
    {
        // $id = \Yii::$app->User->identity->id;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        parent::init();

    }

	public function behaviors()
	{
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	    	'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
	}

    public function actions()
    {
        $actions = parent::actions();
         unset($actions['index']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
     
    public function actionIndex(){
        return Configuracao::find()->where(['=','id', 1])->one();
    }
  
}  