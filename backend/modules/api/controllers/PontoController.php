<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Ponto;
use common\models\CondutorRota;

class PontoController extends ActiveController
{
    public $modelClass = 'common\models\Ponto';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        // unset($actions['index']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
    
    public function actionCreate(){
        $dados = \Yii::$app->request->post();
        return $dados;
        // $rota = new CondutorRota();
        // $rota->descricao = $dados['']
    }
}  