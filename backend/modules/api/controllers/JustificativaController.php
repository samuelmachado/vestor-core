<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Justificativa;

class JustificativaController extends ActiveController
{
    public $modelClass = 'common\models\Justificativa';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        //unset($actions['index']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }

    public function actionCondutor(){
        return Justificativa::find()
                                ->where(['classificacao' => Justificativa::CLASSIFICACAO_CONDUTOR])
                                ->all();
    }
    
    public function actionResponsavel(){
        return Justificativa::find()
                                ->where(['classificacao' => Justificativa::CLASSIFICACAO_RESPONSAVEL])
                                ->all();
    }

    
    

}   