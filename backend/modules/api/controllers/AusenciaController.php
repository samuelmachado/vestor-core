<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Ausencia;
use common\models\CondutorRota;
class AusenciaController extends ActiveController
{
    public $modelClass = 'common\models\Ausencia';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['index']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
    public function actionCondutorAluno($idCondutor){
        $rotas = CondutorRota::find()->where(['idCondutor' => $idCondutor])->all();
        $out = [];
        foreach ($rotas as $rota) {
            if($rota->ausencia)
                $out[] = $rota->ausencia;
        }
        return $out;
    }
    
    public function actionAluno($idAluno){
        $ausencias = Ausencia::find()->where(['idAluno' => $idAluno])->all();
        $output = [];
        
        foreach (Ausencia::ARRAY_ENVIADO as $key => $value) {
            $output[] = $key;
        }

        foreach ($ausencias as $ausencia) {
            $output[$ausencia->enviadoPor] = $ausencia;
        }

        return $output;
    }
    

}   