<?php

namespace backend\modules\api\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Documento;
use common\models\Ocorrencia;
date_default_timezone_set('America/Sao_Paulo');
class DocumentoController extends ActiveController
{
    public $modelClass = 'common\models\Documento';

	// public function behaviors()
	// {
	//     $behaviors = parent::behaviors();
	//     $behaviors['authenticator'] = [
	//         // 'class' => HttpBasicAuth::className(),
	//     	'class' => QueryParamAuth::className(),
	//     ];
	//     return $behaviors;
	// }
    public function base64_to_jpeg($base64_string, $output_file) {
        $path="../../frontend/web/documentos/";
        // open the output file for writing
        $ifp = fopen( $path.$output_file, 'wb' ); 

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
        if(count($data)>1) {
            $dataText=$data[ 1 ];
        } else {
            $dataText=$base64_string;
        }

        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $dataText ) );

        // clean up the file resource
        fclose( $ifp ); 

        return $output_file; 
    }
	public function actionUpload()
	{

        $_POST = json_decode(file_get_contents('php://input'), true);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		$model = Ocorrencia::findOne($_POST['idOcorrencia']);
		$response = ['status' => true];
		$num = 1;
	    foreach ($_POST['arquivos'] as $foto)
	     {
	            $arquivo = 'ocorrencia_'.$model->id.'_foto_'.$num.'_'.time().'.jpg';
	            $this->base64_to_jpeg($foto['arquivo'], $arquivo);
	            $num++;
	            $modelMidia = new Documento;
	            $modelMidia->idOcorrencia = $model->id;
	            $modelMidia->arquivo = 'documentos/'.$arquivo;
	            if (!$modelMidia->save())
	            {
	              $response = [
	                'status' => false,
	                'message' => 'Erro ao salvar arquivo '.($num-1),
	                'data' => $modelMidia->getErrors(),
	              ];
	            }
	     }

	     return $response;
	}
}