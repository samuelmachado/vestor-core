<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Escola;

class AlunoController extends ActiveController
{
    public $modelClass = 'common\models\Aluno';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['index']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
}  