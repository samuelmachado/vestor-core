<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Veiculo;
use common\models\Configuracao;
use common\models\Reserva;
use yii\db\conditions\BetweenColumnsCondition;

class VeiculoController extends ActiveController
{
    public $modelClass = 'common\models\Veiculo';

    // public function behaviors()
    // {
    //     $behaviors = parent::behaviors();
    //     $behaviors['authenticator'] = [
    //         'class' => QueryParamAuth::className(),
    //     ];
    //     return $behaviors;
    // }

    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['create']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
    public function actionDisponivel(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $_POST = json_decode(file_get_contents('php://input'), true);
      $idOrigem = $_POST['destination']['id'];
      $checkIn = explode('T', $_POST['fullCheckIn']);
      $checkOut = explode('T', $_POST['dateCheckOut']);

      $reservas = Reserva::find()->select('idVeiculo')->andWhere(['>=','inicioReserva',$checkIn[0].' '.$_POST['timeCheckIn']])
      ->andWhere(['=','idUnidadeOrigem',$idOrigem])
      ->all(); 

      $ids = array_column($reservas,'idVeiculo');
      $veiculos = Veiculo::find()->andWhere(['not in','idVeiculo', $ids])->andWhere(['=','idUnidade',$idOrigem])->all();
 
      $marcas = [];
      $marcasOut = [];
      foreach ($veiculos as $veiculo) {
        if(!in_array($veiculo->modelo->fabricante->nome, $marcas)){
                $marcas[] = $veiculo->modelo->fabricante->nome;
                $marcasOut[] = $veiculo->modelo->fabricante;
        }
  
      }



     // 
      //->andWhere(['<=','fimReserva',$checkOut[0].' '.$_POST['timeCheckOut']])->all();

      return [
      'status' => true,
      'disponiveis' => $veiculos,
      'fabricantes' => $marcasOut,
    
      ];
      // return $checkIn;
      // return Veiculo::find()->andWhere([''])
      // return $idOrigem;
     // return Veiculo::find()->andWhere([])->all();     
    }

    public function actionTeste(){

      $_POST = json_decode(file_get_contents('php://input'), true);
      $checkIn = explode('T', $_POST['fullCheckIn']);
            $idOrigem = $_POST['destination']['id'];

      $checkOut = explode('T', $_POST['dateCheckOut']);
      $reservas2 = Reserva::find()->andWhere(new BetweenColumnsCondition($checkIn[0].' '.$_POST['timeCheckIn'], 'BETWEEN', 'inicioReserva', 'fimReserva'))
      ->andWhere(new BetweenColumnsCondition($checkOut[0].' '.$_POST['timeCheckOut'], 'BETWEEN', 'inicioReserva', 'fimReserva'))
      // $reservas2 = Reserva::find()->select('idVeiculo')->andFilterWhere(['between', 'inicioReserva',$checkIn[0].' '.$_POST['timeCheckIn'],$checkOut[0].' '.$_POST['timeCheckOut']])
      // ->andFilterWhere(['between', 'fimReserva',$checkIn[0].' '.$_POST['timeCheckIn'],$checkOut[0].' '.$_POST['timeCheckOut']])
      ->andWhere(['=','idUnidadeOrigemx',$idOrigem])
      ->all(); 

      $ids2 = array_column($reservas2,'idVeiculo');
      $veiculos2 = Veiculo::find()->andWhere(['not in','idVeiculo', $ids2])->andWhere(['=','idUnidade',378])->all();

      return $reservas2;
    }
    
    // public function actionAtualizarVeiculo(){
    //           date_default_timezone_set('America/Sao_Paulo');
    //           $config =  Configuracao::setup();
    //           $url = $config->apiStcLink.'getClientVehicles';
    //           $ch = curl_init(); 
    //           $post = [
    //             'key' => $config->apiStcKey,
    //             'user' => $config->apiStcUser,
    //             'pass' => $config->apiStcPass,
    //           ];
    //           curl_setopt($ch, CURLOPT_URL, $url);
    //           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //           curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    //           curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    //           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    //           $response = curl_exec($ch);
    //           curl_close($ch);

    //           $response = json_decode($response, true);
    //           if($response['success']){
    //             foreach ($response['data'] as $data) {
    //               $veiculo = Veiculo::find()->where(['REPLACE(placa, "-","")' => $data['plate'] ])->one();
                  
    //               if($veiculo) {    
    //                 $veiculo->lat = $data['latitude'];
    //                 $veiculo->lng = $data['longitude'];
    //                 $veiculo->ultimaAtualizacaoGPS = $data['date'];
    //                 $veiculo->enderecoGPS = $data['address'];
    //                 $veiculo->ultimaAtualizacaoCRON = date('Y-m-d H:i:s');
    //                 $veiculo->save();
    //               }

    //             }
    //             return ['status' => true];
    //           } else {
    //             return ['status' => false];
    //           }
    //           //print_r($response);
    // }

}  



// DECIMAL lat 10,8
// DECIMAL lng 10,8


    // public function actionAtualizarVeiculo(){
    //           $config =  Configuracao::setup();
    //           $ch = curl_init();
    //           $post = [
    //             'key' => $config->apiStcKey,
    //             'user' => $config->apiStcUser,
    //             'pass' => $config->apiStcPass,
    //           ];
    //           curl_setopt($ch, CURLOPT_URL, $config->apiStcLink);
    //           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //           curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    //           curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    //           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    //           $response = curl_exec($ch);
    //           curl_close($ch);

    //           $response = json_decode($response, true);
    //           print_r($response);
    //           print '<Br><br><br>';
    //           if($response['success']){
    //             foreach ($response['data'] as $data) {
    //                 print_r($data);
    //             }
    //           }
    // }