<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Escola;

class EscolaController extends ActiveController
{
    public $modelClass = 'common\models\Escola';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
    public function actionIndex(){
        $escolas = Escola::find()->all();
        foreach ($escolas as $escola) {
            $escola->alunosEscola = $escola->alunos;
        }
        return $escolas;
    }

    public function actionAluno($id){
        $escola = Escola::findOne($id);
        
        if(!$escola)
            return [];
        return $escola->alunos;
    }
}  