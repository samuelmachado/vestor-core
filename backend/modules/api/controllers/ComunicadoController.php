<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Comunicado;
use common\models\CondutorRota;
use common\models\Usuario;
use common\models\Aluno;
use common\models\AlunoRota;
use common\models\PontoAluno;
class ComunicadoController extends ActiveController
{
    public $modelClass = 'common\models\Comunicado';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
         unset($actions['create']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    public function actionCreate(){
          $dados = json_decode(file_get_contents('php://input'), true);
          $model = new Comunicado();
          $model->idAluno = $dados['idAluno'];
          $model->data = $dados['data'];
          $model->idJustificativa = $dados['idJustificativa'];
          $model->tipo = $dados['tipo'];
          $model->enviadoPor = $dados['enviadoPor'];

          $pontoAluno = PontoAluno::find()->where(['idAluno' => $model->idAluno])->one();
          if($pontoAluno){
              $model->idCondutor = $pontoAluno->ponto->condutorRota->idCondutor;
              if(!$model->save()){
                return [
                    'status' => false,
                    'errors' => $model->getErrors(),
                ];
              }
          } else {
            return ['status' => false, 'errors' => [], 'message' => 'Aluno não vinculado a uma rota'];
          }
      
          return ['status' => true];
    }
    public function actionAusenciaMultipla(){
          $dados = json_decode(file_get_contents('php://input'), true);

           if($dados['comunicados']){
              foreach ($dados['comunicados'] as $comunicado) {
                $model = new Comunicado();
                $model->idAluno = $comunicado['idAluno'];
                $model->idCondutor = $comunicado['idCondutor'];
                $model->data = $comunicado['data'];
                $model->idJustificativa = $comunicado['idJustificativa'];
                $model->tipo = $comunicado['tipo'];
                $model->enviadoPor = $comunicado['enviadoPor'];
                $pontoAluno = PontoAluno::find()->where(['idAluno' => $model->idAluno])->one();
                  // if($pontoAluno){
                      // $model->idCondutor = $pontoAluno->ponto->condutorRota->idCondutor;
                      if(!$model->save()){
                        return [
                            'status' => false,
                            'errors' => $model->getErrors(),
                        ];
                      }
                  // } else {
                  //   return ['status' => false, 'errors' => [], 'message' => 'Aluno não vinculado a uma rota'];
                  // }
              }
              return ['status' => true];
           }
           return ['status' => false];


          // $pontoAluno = PontoAluno::find()->where(['idAluno' => $model->idAluno])->one();
          // if($pontoAluno){
          //     $model->idCondutor = $pontoAluno->ponto->condutorRota->idCondutor;
          //     if(!$model->save()){
          //       return [
          //           'status' => false,
          //           'errors' => $model->getErrors(),
          //       ];
          //     }
          // } else {
          //   return ['status' => false, 'errors' => [], 'message' => 'Aluno não vinculado a uma rota'];
          // }
      
          // return ['status' => true];

    }
    public function actionCondutorAluno($idCondutor){
        $comunicados = Comunicado::find()->where(['idCondutor' => $idCondutor])->all();
  
        return $comunicados;
    }
    
    public function actionAluno($idAluno){
        $ausencias = Comunicado::find()->where(['idAluno' => $idAluno])->all();
        $output = [];
        
        foreach (Comunicado::ARRAY_ENVIADO as $key => $value) {
            $output[] = $key;
        }

        foreach ($ausencias as $ausencia) {
            $output[$ausencia->enviadoPor] = $ausencia;
        }

        return $output;
    }
    
        public function actionResponsavel($idUsuario){
        $usuario = Usuario::findOne($idUsuario);

        if($usuario->cpf){
            $filhos = Aluno::find()
                                ->where(['REPLACE(REPLACE(cpfResponsavel, ".",""),"-","")' => $usuario->cpf])
                                ->all(); 
           
                
            return ['status' => true, 'data' => $filhos, 'message' => 'OK!'];
       

        }
        return ['status' => false, 'message' => 'CPF inexistente/inválido'];
        
    }

}   