<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Reserva;
date_default_timezone_set('America/Sao_Paulo');
class ReservaController extends ActiveController
{
    public $modelClass = 'common\models\Reserva';

    public function init()
    {
        $id = \Yii::$app->User->identity->id;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        parent::init();
        
    }

	public function behaviors()
	{
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	    	'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
         unset($actions['index']);
        // unset($actions['delete']);
        return $actions;
    }
    public function actionIndex(){
        $id = \Yii::$app->User->identity->id;
        $reservas = Reserva::find()
        ->where(['=','idUsuario', $id])
        ->andWhere(['is', 'checkOut', null])
        
        //->andWhere(['<=', 'checkIn',  date('Y-m-d H:i:s',(strtotime ( '-1 day' , strtotime (date('Y-m-d H:i:s')) ) )) ])
        
        ->orderBy(['inicioReserva'=>SORT_ASC])->all();
        return $this->groupByDay($reservas);
    }

    public function actionHistorico(){
        $id = \Yii::$app->User->identity->id;
        $reservas = Reserva::find()
            ->where(['=','idUsuario', $id])
            ->andWhere(['is not', 'checkOut', null])
            ->orderBy(['inicioReserva'=> SORT_DESC])->all();  

        return $this->groupByDay($reservas);
    } 
    private function groupByDay($reservas){
        $dias = [];
        // Monto o array somente com os dias ÚNICOS que vem do banco.
        // Ex. $dias = [0 => 10/10/10, 1 => 11/10/10];
        foreach ($reservas as $reserva) { 
            $start =  date('Y-m-d',strtotime($reserva->inicioReserva) );
            if(!in_array($start, $dias)){
                 array_push($dias, $start);
            } 
        } 

        $response = [];
        //  Itero sobre cada DIA e em cada dia eu itero sobre cada reserva 
        // para alocá-la em sua respectiva posição no array dias
        // Ex. $response = [..., ['10/10/10', [reserva1, reserva2, reserva3]]];

        foreach ($dias as $dia) {
            $reservasDoDia = [];
            foreach ($reservas as $reserva) {
                if($dia == date('Y-m-d',strtotime($reserva->inicioReserva))){
                    array_push( $reservasDoDia, $reserva);
                }
            }
           array_push($response, [$dia, $reservasDoDia]);
        }
        return $response;
    }
    public function actionCreate(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        $startDate = date("Y-m-d H:i:s", strtotime($_POST['fullCheckIn']));
        $endDate = date("Y-m-d H:i:s", strtotime($_POST['fullCheckOut']));

        // $model = new Reserva();
        $reservas = Reserva::find()
                                ->where(['>=','fimReserva',$startDate])
                                ->andWhere(['<=','inicioReserva',$endDate])
                                ->andWhere(['=','idVeiculo',$_POST['car']['id']])
                                ->all();
        if($reservas){
            return ['status' => false, 'message' => 'Infelizmente este veículo acabou de ser agendado nesse horário'];
        } else {
            $model = new Reserva();
            $model->idUsuario =  \Yii::$app->User->identity->id;
            $model->idVeiculo = $_POST['car']['id'];
            $model->inicioReserva = $startDate;
            $model->fimReserva = $endDate;
            $model->idUnidadeOrigem = $_POST['origin']['id'];
            $model->idUnidadeTermino = $_POST['destination']['id'];
            if(!$model->save()){
                return [
                    'status' => false,
                     'message' => 'Erro ao realizar reserva',
                     'data' => print_r($model->getErrors(), true)
                  ];
            } else {
                return [
                    'status' => true,
                     'message' => 'Realizado com sucesso',
                     'data' => $model
                  ];
            }
        }
    }

    public function actionCheckout($id){
        $model = Reserva::find()->where(['=','id', $id])->one();
        $model->checkOut = date('Y-m-d H:i:s');
         if(!$model->save()){
            return [
                'status' => false,
                 'message' => 'Erro ao realizar checkout',
                 'data' => print_r($model->getErrors(), true)
              ];
        } else {
            return [
                'status' => true,
                 'message' => 'Realizado com sucesso',
                 'data' => $model
              ];
        }
    }

     public function actionCancelar($id){
        $model = Reserva::find()->where(['=','id', $id])->one();
   
         if(!$model->delete()){
            return [
                'status' => false,
                 'message' => 'Erro ao realizar cancelar reserva',
                 'data' => print_r($model->getErrors(), true)
              ];
        } else {
            return [
                'status' => true,
                 'message' => 'Realizado com sucesso',
                 'data' => $model
              ];
        }
    }


     public function actionCheckin($id){
        $model = Reserva::find()->where(['=','id', $id])->one();
        if(date('Y-m-d H:i:s') < $model->inicioReserva)
          return ['status' => false, 'message' => 'A Reserva só inicia em '.date("d/m/y H:i", strtotime($model->inicioReserva)) ];
        if(!$model->checkIn || $model->checkIn == '0000-00-00 00:00:00'){
            $model->checkIn = date('Y-m-d H:i:s');
             if(!$model->save()){
                return [
                    'status' => false,
                     'message' => 'Erro ao realizar checkIn',
                     'data' => print_r($model->getErrors(), true)
                  ];
            } else {
                return [
                    'status' => true,
                     'message' => 'Realizado com sucesso',
                     'data' => $model
                  ];
            }
        } else {
                return [ 
                    'status' => false,
                     'message' => 'O CheckIn já foi realizado anteriormente.',
                     'data' => $model
                  ];
        }
    
    }
 
  
}  