<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Condutor;

class CondutorController extends ActiveController
{
    public $modelClass = 'common\models\Condutor';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['index']);
        // unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
    public function actionEscolaCondutor($id){
        $condutor = Condutor::findOne($id);
        if(!$condutor)
            return [];
        return $condutor->escolas;
    }

    public function actionEscolas(){
        
        $condutores = Condutor::find()->all();
        
        return $condutores ? $condutores : [];
    }
}   