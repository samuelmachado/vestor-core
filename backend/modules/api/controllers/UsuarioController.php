<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Usuario;
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class UsuarioController extends ActiveController
{
    public $modelClass = 'common\models\Usuario';

    public function init()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        parent::init();

    }

	public function behaviors()
	{
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	    	'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
	}

    public function actions()
    {
        $actions = parent::actions();
         // unset($actions['index']);
         unset($actions['update']);
        // unset($actions['delete']);
        return $actions;
    }
    
    public function actionUpdate(){
        $id = \Yii::$app->User->identity->id;
        $_POST = json_decode(file_get_contents('php://input'), true);

        $model = Usuario::find()->where(['=','id', $id])->one();
        if(isset($_POST['senha']) && $_POST['senha']){
             $model->setPassword($_POST['senha']);
        }
        if(isset($_POST['email'])){
            $model->email = $_POST['email'];
        }
        if(isset($_POST['nome'])){
            $model->nome = $_POST['nome'];
        }
           if(!$model->save()){
                return [
                    'status' => false,
                     'message' => 'Erro ao atualizar dados',
                     'data' => print_r($model->getErrors(), true),
                     'dataJson' => $model->getErrors()
                  ];
            } else {
                return [
                    'status' => true,
                     'message' => 'Atualizado com sucesso',
                     'data' => $model
                  ];
            }
    }


    public function base64_to_jpeg($base64_string, $output_file) {
        $path="../../frontend/web/usuarios/";
        // open the output file for writing
        $ifp = fopen( $path.$output_file, 'wb' ); 

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
        if(count($data)>1) {
            $dataText=$data[ 1 ];
        } else {
            $dataText=$base64_string;
        }

        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $dataText ) );

        // clean up the file resource
        fclose( $ifp ); 

        return $output_file; 
    }
    public function actionUpload()
    {   
     $id = \Yii::$app->User->identity->id;
        $_POST = json_decode(file_get_contents('php://input'), true);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = Usuario::findOne($id);
        $response = ['status' => true];
    
         $arquivo = 'perfil_'.$model->id.'_foto_'.time().'.jpg';
          $this->base64_to_jpeg($_POST['foto'], $arquivo);
           
            
            $model->imagem =  'usuarios/'.$arquivo;
            if (!$model->save())
            {
              $response = [
                'status' => false,
                'message' => 'Erro ao salvar arquivo '.($num-1),
                'data' => $model->getErrors(),
              ];
            } else {
                  $response = ['status' => true, 'data' => $model];
            }

         return $response;
    }
  
}  