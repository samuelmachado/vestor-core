<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fabricante".
 *
 * @property int $id Código
 * @property string $nome Nome
 *
 * @property Veiculo[] $veiculos
 */
class Fabricante extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fabricante';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'nome' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeiculos()
    {
        return $this->hasMany(Veiculo::className(), ['idFabricante' => 'id']);
    }

    public static function fabricantes(){
         return static::findAll();
    }

}
