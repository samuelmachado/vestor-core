<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Solicitacao;

/**
 * SolicitacaoSearch represents the model behind the search form of `common\models\Solicitacao`.
 */
class SolicitacaoSearch extends Solicitacao
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'idCliente', 'idLocutor'], 'integer'],
            [['descricao', 'anexo', 'dataHora', 'dataHoraEnvio', 'dataHoraGravacao'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Solicitacao::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'idCliente' => $this->idCliente,
            'idLocutor' => $this->idLocutor,
            'dataHora' => $this->dataHora,
            'dataHoraEnvio' => $this->dataHoraEnvio,
            'dataHoraGravacao' => $this->dataHoraGravacao,
        ]);

        $query->andFilterWhere(['like', 'descricao', $this->descricao])
            ->andFilterWhere(['like', 'anexo', $this->anexo]);

        return $dataProvider;
    }
}
