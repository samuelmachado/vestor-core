<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "postoscredenciados".
 *
 * @property int $id
 * @property string $cnpj
 * @property string $nomeFantasia
 * @property string|null $lat
 * @property string|null $long
 * @property string|null $endereco
 * @property int $idUnidade
 * @property int $idBandeira
 *
 * @property Bandeira $idBandeira0
 * @property Unidade $idUnidade0
 */
class PostosCredenciados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'postoscredenciados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cnpj', 'nomeFantasia', 'idUnidade', 'idBandeira'], 'required'],
            [['idUnidade', 'idBandeira'], 'integer'],
            [['cnpj', 'lat', 'long'], 'string', 'max' => 20],
            [['nomeFantasia', 'endereco'], 'string', 'max' => 255],
            [['idBandeira'], 'exist', 'skipOnError' => true, 'targetClass' => Bandeira::className(), 'targetAttribute' => ['idBandeira' => 'id']],
            [['idUnidade'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['idUnidade' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cnpj' => 'CNPJ',
            'nomeFantasia' => 'Nome Fantasia',
            'lat' => 'Endereço',
            'long' => 'Long',
            'endereco' => 'Endereço',
            'idUnidade' => 'Unidade',
            'idBandeira' => 'Bandeira',
        ];
    }

    /**
     * Gets query for [[IdBandeira0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdBandeira0()
    {
        return $this->hasOne(Bandeira::className(), ['id' => 'idBandeira']);
    }

    /**
     * Gets query for [[IdUnidade0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUnidade0()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'idUnidade']);
    }
}
