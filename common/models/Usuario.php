<?php

namespace common\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id Cód.
 * @property int $idPerfil Perfil
 * @property string $nome Nome
 * @property string $email Email
 * @property string $imagem Imagem
 * @property string $authKey Key
 * @property string $passwordHash Senha
 * @property string $passwordResetToken Reset
 * @property string $idFirebase Firebase
 * @property int $status Ativo
 * @property string $created_at Create
 * @property string $updated_at Update
 *
 * @property Solicitacao[] $solicitacaos
 * @property Solicitacao[] $solicitacaos0
 */
class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $password;

    const STATUS_DELETED = 0;
    const STATUS_ATIVO = 1;
    const STATUS_PENDENTE = 2;

    const PERFIL_SUPER_ADMIN = 1;
    const USUARIO = 2;
    const PORTARIA = 3;

    const ARRAY_PERFIS = [
        5 => 'APR',
        12 => 'CSC',
        4 => 'Financeiro',
        6 => 'Gente e Gestão',
        11 => 'Gerente de vendas',
        8 => 'Marketing Comercial',
        3 => 'Portaria',
        1 => 'Super Admin',
        10 => 'Supervisor de vendas',
        7 => 'Trade',
        2 => 'Usuário do App',
        9 => 'Vendedor',
    ];
    const ARRAY_STATUS = [
        1 => 'Ativo',
        0 => 'Banido',
    ];

    const ARRAY_ROTAS = [
        1 => 'Sim',
        0 => 'Não',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPerfil', 'status'], 'integer'],
            [['email', 'authKey', 'passwordHash','password','nome','idPerfil','status','idTipoLicenca'], 'required'],
            [['created_at', 'updated_at', 'password', 'username', 'primeiroAcesso','chaveDallas','rotaCritica'], 'safe'],
            [['nome'], 'string', 'max' => 80],
            [['chaveDallas'], 'string', 'max' => 550],
            [['email', 'passwordHash', 'passwordResetToken', 'idFirebase'], 'string', 'max' => 255],
            [['imagem'], 'string', 'max' => 200],
            [['email'], 'email'],
            [['authKey'], 'string', 'max' => 320],
            [['email'], 'unique'],
            [['passwordResetToken'], 'unique'],
            [['idTipoLicenca'], 'exist', 'skipOnError' => true, 'targetClass' => TipoLicenca::className(), 'targetAttribute' => ['idTipoLicenca' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idPerfil' => 'Perfil',
            'nome' => 'Nome',
            'email' => 'Email',
            'imagem' => 'Imagem',
            'authKey' => 'Key',
            'passwordHash' => 'Senha',
            'passwordResetToken' => 'Token da Senha',
            'idFirebase' => 'Firebase',
            'status' => 'Ativo',
            'created_at' => 'Criado',
            'updated_at' => 'Editado',
            'password' => 'Senha',
            'username' => 'Login',
            'idTipoLicenca' => 'Tipo de licença',
            'chaveDallas' => 'Chave dallas',
            'rotaCritica' => 'Rota crítica'

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitacaos()
    {
        return $this->hasMany(Solicitacao::className(), ['idCliente' => 'id']);
    }

    public function getTipoLicenca()
    {
        return $this->hasOne(TipoLicenca::className(), ['id' => 'idTipoLicenca']);
    }

    public static function findByUsername($username)
    {
        //'status' => self::STATUS_ATIVO]
        return static::findOne(['email' => $username]);
    }

    public static function findByLogin($username)
    {
        //'status' => self::STATUS_ATIVO]
        return static::findOne(['email' => $username]);
    }


    public function getAuthKey()
    {
        return $this->authKey;
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }


    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //echo Yii::$app->security->generatePasswordHash($password);

        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ATIVO, 'idPerfil' => self::PERFIL_SUPER_ADMIN]);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['authKey' => $token]);
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function setPassword($password)
    {
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'passwordResetToken' => $token,
            'status' => self::STATUS_ATIVO,
        ]);
    }
    public function removePasswordResetToken()
    {
        $this->passwordResetToken = null;
    }
    // public static function clientes(){
    //      return static::findAll(['idPerfil' => self::PERFIL_CLIENTE]);
    // }

    // public static function locutores(){
    //      return static::findAll(['idPerfil' => self::PERFIL_LOCUTOR]);
    // }


    public function getMeuPerfil()
    {

        return (self::ARRAY_PERFIS[$this->idPerfil]);
    }

    public function getMeuStatus()
    {

        return (self::ARRAY_STATUS[$this->status]);
    }

    public function getMinhaRota()
    {

        return (self::ARRAY_ROTAS[$this->rotaCritica]);
    }

    public function getVeiculo()
    {
        return $this->hasMany(Veiculo::className(), ['idDonoVeiculo' => 'id']);
    }

}
