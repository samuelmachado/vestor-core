<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "regional".
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property string $imagem
 * @property int $qtdUnidade
 */
class Regional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'regional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            // [['imagem'], 'required', 'message'=>"Por favor faça o carregamento de um arquivo."],
            [['nome'], 'string', 'max' => 225],
            [['descricao'], 'string', 'max' => 255],
            // [['imagem'], 'string', 'max' => 225],
            [['qtdUnidade'], 'safe'],
            [['imagem'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'descricao' => 'Descrição',
            'imagem' => 'Imagem',
            'qtdUnidade' => 'Quantidade de unidades'
        ];
    }

    public function getUnidades()
    {
        return $this->hasMany(Unidade::className(), ['idRegional' => 'id']);
    }
}
