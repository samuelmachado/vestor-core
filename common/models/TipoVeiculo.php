<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tipoVeiculo".
 *
 * @property int $id Cód.
 * @property string $nome Nome
 * @property string $imagem Imagem
 * @property int $qtdUnidade
 */
class TipoVeiculo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipoVeiculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 50],
            [['imagem'], 'string', 'max' => 120],
            [['imagem'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['qtdVeiculos'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Modelo/Marca',
            'imagem' => 'Imagem',
            'qtdVeiculos' => 'Quantidade de veículos'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeiculos()
    {
        return $this->hasMany(Veiculo::className(), ['idTipoVeiculo' => 'id']);
    }
}
