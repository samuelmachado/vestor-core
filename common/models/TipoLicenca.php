<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tipoLicenca".
 *
 * @property int $id
 * @property string $nome
 */
class TipoLicenca extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipoLicenca';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 50],
            [['qtdMotorista'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
        ];
    }

    public function getMotoristas()
    {
        return $this->hasMany(Usuario::className(), ['idTipoLicenca' => 'id']);
    }
}
