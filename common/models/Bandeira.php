<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bandeira".
 *
 * @property int $id
 * @property string $nome
 *
 * @property Postoscredenciados[] $postoscredenciados
 */
class Bandeira extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bandeira';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
        ];
    }

    /**
     * Gets query for [[Postoscredenciados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPostoscredenciados()
    {
        return $this->hasMany(Postoscredenciados::className(), ['idBandeira' => 'id']);
    }
}
