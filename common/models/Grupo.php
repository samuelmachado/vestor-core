<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "grupo".
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 */
class Grupo extends \yii\db\ActiveRecord
{
    public $inputUnidades;
     /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grupo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 50],
            [['descricao'], 'string', 'max' => 255],
            [['inputUnidades'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome do grupo',
            'descricao' => 'Descrição',
            'inputUnidades' => 'Unidades'
        ];
    }

    public function getGrupoUnidade()
    {
        return $this->hasMany(GrupoUnidade::className(), ['idGrupo' => 'id']);
    }

    public function getUnidades()
    {
        return $this->hasMany(Unidade::className(), ['id' => 'idUnidade'])
            ->viaTable('grupoUnidade', ['idGrupo' => 'id']);
    }
}
