<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reserva".
 *
 * @property string $id Ocorrência
 * @property string $idUsuario Usuário
 * @property string $inicioReserva Reserva Início
 * @property string $fimReservsa Reserva Entrega
 * @property string $checkIn Check in
 * @property string $checkOut Check out
 * @property string $inicioKm Kilometragem Início
 * @property string $fimKm Kilometragem Fim
 * @property int $idUnidadeOrigem Origem
 * @property int $idUnidadeTermino Destino Original
 * @property int $idUnidadeTerminoReal Destino Real
 * @property string $cadastrado Data do Cadastro
 *
 * @property Reserva $usuario
 * @property Reserva[] $reservas
 * @property Unidade $unidadeOrigem
 * @property Unidade $unidadeTermino
 * @property Unidade $unidadeTerminoReal
 */
class Reserva extends \yii\db\ActiveRecord
{
    public $inicioReserva2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reserva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idUsuario', 'inicioReserva', 'fimReserva', 'idUnidadeOrigem', 'idUnidadeTermino','idVeiculo'], 'required'],
            [['idUsuario', 'inicioKm', 'fimKm', 'idUnidadeOrigem', 'idUnidadeTermino', 'idUnidadeTerminoReal'], 'integer'],
            [['inicioReserva', 'fimReserva', 'checkIn', 'checkOut', 'cadastrado'], 'safe'],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'id']],
            [['idUnidadeOrigem'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['idUnidadeOrigem' => 'id']],
            [['idUnidadeTermino'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['idUnidadeTermino' => 'id']],
            [['idUnidadeTerminoReal'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['idUnidadeTerminoReal' => 'id']],
            [['idVeiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Veiculo::className(), 'targetAttribute' => ['idVeiculo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código da reserva',
            'idUsuario' => 'Usuário',
            'inicioReserva' => 'Reserva Início',
            'fimReserva' => 'Reserva Entrega',
            'checkIn' => 'Check in',
            'checkOut' => 'Check out',
            'inicioKm' => 'Kilometragem Início',
            'fimKm' => 'Kilometragem Fim',
            'idUnidadeOrigem' => 'Origem',
            'idUnidadeTermino' => 'Destino Original',
            'idUnidadeTerminoReal' => 'Destino Real',
            'cadastrado' => 'Data do Cadastro',
        ];
    }
   public function fields()
     {
        $fields = parent::fields();

        $fields['veiculo'] = 'veiculo';
        $fields['unidadeOrigem'] = 'unidadeOrigem';
        $fields['unidadeDestino'] = 'unidadeTermino';
        $fields['usuario'] = 'usuario';

        return $fields;
      }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOcorrencias()
    {
        return $this->hasMany(Ocorrencia::className(), ['idReserva' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidadeOrigem()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'idUnidadeOrigem']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidadeTermino()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'idUnidadeTermino']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidadeTerminoReal()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'idUnidadeTerminoReal']);
    }

        public function getVeiculo()
    {
        return $this->hasOne(Veiculo::className(), ['id' => 'idVeiculo']);
    }

    /**
     * Método que retorna a quantidade dos ultimos 3 meses de reservas
     * @return integer
     */
    public function getQtd3Mouth($id){

        $dataNow = new \DateTime();
        $dataNow->sub(new \DateInterval('P3M'));
        $Mouth3Ago = date_format($dataNow, 'Y-m-d');

        return count(Reserva::find()->where(['idUsuario'=>$id])->andFilterWhere(['between', 'inicioReserva', $Mouth3Ago,date_format( new \DateTime(),'Y-m-d')])->all());
    }

    /**
     * Método que retorna a quantidade dos ultimos 6 meses de reservas
     * @return integer
     */
    public function getQtd6Mouth($id){

        $dataNow = new \DateTime();
        $dataNow->sub(new \DateInterval('P6M'));
        $Mouth3Ago = date_format($dataNow, 'Y-m-d');

        return count(Reserva::find()->where(['idUsuario'=>$id])->andFilterWhere(['between', 'inicioReserva', $Mouth3Ago,date_format( new \DateTime(),'Y-m-d')])->all());
    }

    /**
     * Método que retorna a quantidade dos ultimos 12 meses de reservas
     * @return integer
     */
    public function getQtd12Mouth($id){

        $dataNow = new \DateTime();
        $dataNow->sub(new \DateInterval('P12M'));
        $Mouth3Ago = date_format($dataNow, 'Y-m-d');

        return count(Reserva::find()->where(['idUsuario'=>$id])->andFilterWhere(['between', 'inicioReserva', $Mouth3Ago,date_format( new \DateTime(),'Y-m-d')])->all());
    }
}
