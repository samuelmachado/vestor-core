<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "localizacaoveiculo".
 *
 * @property string $id
 * @property string $idVeiculo
 * @property int $idUnidade
 * @property string $registrado
 *
 * @property Unidade $unidade
 * @property Veiculo $veiculo
 */
class LocalizacaoVeiculo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'localizacaoveiculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idVeiculo', 'idUnidade'], 'required'],
            [['idVeiculo', 'idUnidade'], 'integer'],
            [['registrado'], 'safe'],
            [['idUnidade'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['idUnidade' => 'id']],
            [['idVeiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Veiculo::className(), 'targetAttribute' => ['idVeiculo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idVeiculo' => 'Id Veiculo',
            'idUnidade' => 'Id Unidade',
            'registrado' => 'Registrado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidade()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'idUnidade']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeiculo()
    {
        return $this->hasOne(Veiculo::className(), ['id' => 'idVeiculo']);
    }
}
