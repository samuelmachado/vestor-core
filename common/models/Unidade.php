<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unidade".
 *
 * @property int $id Código
 * @property string $nome Nome
 * @property string $endereco Endereço
 * @property string $lat Latitude
 * @property string $lng Longitude
 *
 * @property Ocorrencia[] $ocorrencias
 * @property Reserva[] $reservas
 * @property Reserva[] $reservas0
 * @property Reserva[] $reservas1
 */
class Unidade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unidade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['nome', 'endereco', 'lat', 'lng','idRegional'], 'required'],
            [['nome', 'endereco', 'idRegional'], 'required'],
            [['lat', 'lng'], 'number'],
            [['nome', 'endereco'], 'string', 'max' => 200],
            [['idRegional'], 'exist', 'skipOnError' => true, 'targetClass' => Regional::className(), 'targetAttribute' => ['idRegional' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'nome' => 'Nome da unidade',
            'endereco' => 'Endereço',
            'lat' => 'Latitude',
            'lng' => 'Longitude',
            'idRegional' => 'Regional'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOcorrencias()
    {
        return $this->hasMany(Ocorrencia::className(), ['idReserva' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['idUnidadeOrigem' => 'id']);
    }


    public function getVeiculos()
    {
        return $this->hasMany(Veiculo::className(), ['idUnidade' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas0()
    {
        return $this->hasMany(Reserva::className(), ['idUnidadeTermino' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas1()
    {
        return $this->hasMany(Reserva::className(), ['idUnidadeTerminoReal' => 'id']);
    }

    public function getRegional()
    {
        return $this->hasOne(Regional::className(), ['id' => 'idRegional']);
    }
}
