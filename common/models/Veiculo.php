<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "veiculo".
 *
 * @property string $id Código
 * @property int $idUnidade Unidade
 * @property int $idModelo Modelo
 * @property string $placa Placa
 * @property string $modelo Modelo
 * @property string $cor Cor
 *
 * @property Ocorrencia[] $ocorrencias
 * @property Fabricante $fabricante
 */
class Veiculo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'veiculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idTipoVeiculo','idDonoVeiculo', 'emUso', 'placa'], 'required'],
            [['horarioInicio','horarioFinal','idTipoVeiculo','idDonoVeiculo', 'emUso'], 'integer'],
            [['created_at','updated_at'], 'string'],
            [['idUnidade', 'semParar'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'horarioInicio' => '',
            'horarioFinal' => '',
            'idTipoVeiculo' => 'Tipo de veículo',
            'idDonoVeiculo' => 'Dias disponíveis',
            'emUso' => 'Em uso',
            'idUnidade' => 'Unidade',
            'placa' => 'Placa',
            'semParar' => 'Sem parar',
            // 'diasDisponiveis' => 'Dias disponíveis'
        ];
    }

    // public function fields()
    // {
    //     $fields = parent::fields();

    //     $fields['veiculo'] = 'veiculo';

    //     return $fields;
    // }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDono()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idDonoVeiculo']);
    }

        /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(TipoVeiculo::className(), ['idTipoVeiculo' => 'id']);
    }

}
