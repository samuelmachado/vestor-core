<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ocorrencia".
 *
 * @property string $id Código
 * @property int $idUsuario Usuário
 * @property int $idReserva Reserva
 * @property string $idVeiculo Veículo
 * @property string $descricao Descrição
 * @property string $cadastrado Cadastrado
 *
 * @property Documento[] $documentos
 * @property Unidade $reserva
 * @property Usuario $usuario
 * @property Veiculo $veiculo
 */
class Ocorrencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ocorrencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idUsuario', 'idReserva', 'idVeiculo', 'descricao'], 'required'],
            [['idUsuario', 'idReserva', 'idVeiculo'], 'integer'],
            [['descricao'], 'string'],
            [['cadastrado'], 'safe'],
            [['idReserva'], 'exist', 'skipOnError' => true, 'targetClass' => Reserva::className(), 'targetAttribute' => ['idReserva' => 'id']],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'id']],
            [['idVeiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Veiculo::className(), 'targetAttribute' => ['idVeiculo' => 'id']],
        ];
    }

    public function fields()
     {
        $fields = parent::fields();

        $fields['documentos'] = 'documentos';   

        return $fields;
      }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idUsuario' => 'Usuário',
            'idReserva' => 'Reserva',
            'idVeiculo' => 'Veículo',
            'descricao' => 'Descrição',
            'cadastrado' => 'Cadastrado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documento::className(), ['idOcorrencia' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReserva()
    {
        return $this->hasOne(Reserva::className(), ['id' => 'idReserva']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeiculo()
    {
        return $this->hasOne(Veiculo::className(), ['id' => 'idVeiculo']);
    }
}
