<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "modelo".
 *
 * @property int $id Código
 * @property int $idFabricante Fabricante
 * @property string $modelo Modelo
 * @property string $imagem Imagem
 *
 * @property Fabricante $fabricante
 */
class Modelo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modelo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idFabricante', 'modelo', 'imagem'], 'required'],
            [['idFabricante'], 'integer'],
            [['modelo'], 'string', 'max' => 50],
            [['imagem'], 'string', 'max' => 200],
            [['idFabricante'], 'exist', 'skipOnError' => true, 'targetClass' => Fabricante::className(), 'targetAttribute' => ['idFabricante' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idFabricante' => 'Fabricante',
            'modelo' => 'Modelo',
            'imagem' => 'Imagem',
        ];
    }

     public function fields()
     {
        $fields = parent::fields();

        $fields['fabricante'] = 'fabricante';   

        return $fields;
      }
      
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFabricante()
    {
        return $this->hasOne(Fabricante::className(), ['id' => 'idFabricante']);
    }
}
