<?php

namespace common\models;

use Yii;


class Configuracao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Configuracao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['politicaFrota'], 'required'],
            // [['lat', 'lng'], 'number'],
            // [['nome', 'endereco'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'politicaFrota' => 'Política de Frota'
 
        ];
    }

 
}
