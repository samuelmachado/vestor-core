<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Reserva;

/**
 * ReservaSearch represents the model behind the search form of `common\models\Reserva`.
 */
class ReservaSearch extends Reserva
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'idUsuario', 'inicioKm', 'fimKm', 'idUnidadeOrigem', 'idUnidadeTermino', 'idUnidadeTerminoReal'], 'integer'],
             [['inicioReserva', 'fimReserva', 'checkIn', 'checkOut', 'cadastrado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reserva::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
     
        if ( $this->inicioReserva ) 
        $query->andFilterWhere ( [ '>=' , 'DATE_FORMAT(inicioReserva,"%Y-%m-%d")' , date ( 'Y-m-d' , strtotime ( $this->inicioReserva ) ) ] );
               if ( $this->fimReserva ) 
        $query->andFilterWhere ( [ '<=' , 'DATE_FORMAT(fimReserva,"%Y-%m-%d ")' , date ( 'Y-m-d' , strtotime ( $this->fimReserva ) ) ] );
               if ( $this->checkIn ) 
        $query->andFilterWhere ( [ '<=' , 'DATE_FORMAT(checkIn,"%Y-%m-%d")' , date ( 'Y-m-d' , strtotime ( $this->checkIn ) ) ] );
        
         if ( $this->checkOut ) 
        $query->andFilterWhere ( [ '<=' , 'DATE_FORMAT(checkOut,"%Y-%m-%d")' , date ( 'Y-m-d' , strtotime ( $this->checkOut ) ) ] );
        

    
    // if ( $this->end_date_time !== '' && !is_null($this->end_date_time) ) {
    //     $query->andFilterWhere ( [ '<=' , 'DATE_FORMAT(end_date_time,"%Y-%m-%d %H:%i:%s")' , date ( 'Y-m-d H:i:s' , strtotime ( $this->end_date_time ) ) ] );
    // }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'idUsuario' => $this->idUsuario,
            // 'inicioReserva' => $this->inicioReserva,
            // 'fimReserva' => $this->fimReserva,
            // 'checkIn' => $this->checkIn,
            // 'checkOut' => $this->checkOut,
            'inicioKm' => $this->inicioKm,
            'fimKm' => $this->fimKm,
            'idUnidadeOrigem' => $this->idUnidadeOrigem,
            'idUnidadeTermino' => $this->idUnidadeTermino,
            'idUnidadeTerminoReal' => $this->idUnidadeTerminoReal,
            'cadastrado' => $this->cadastrado,
        ]);

        return $dataProvider;
    }
}
