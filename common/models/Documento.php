<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "documento".
 *
 * @property int $id Cód Arquivo
 * @property int $idSolicitacao Cód. Solicitacao
 * @property string $arquivo Arquivo
 * @property string $data Registrado
 *
 * @property Usuario $usuario
 */
class Documento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idOcorrencia', 'arquivo'], 'required'],
            [['idOcorrencia'], 'integer'],
            [['arquivo'], 'string', 'max' => 255],
            [['idOcorrencia'], 'exist', 'skipOnError' => true, 'targetClass' => Ocorrencia::className(), 'targetAttribute' => ['idOcorrencia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Cód Arquivo',
            'idOcorrencia' => 'Cód. Ocorrência',
            'arquivo' => 'Arquivo',
            'data' => 'Registrado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitacao()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idOcorrencia']);
    }
}
