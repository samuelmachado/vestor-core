<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "grupoUnidade".
 *
 * @property int $id
 * @property int $idGrupo
 * @property int $idUnidade
 */
class GrupoUnidade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grupoUnidade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idGrupo', 'idUnidade'], 'required'],
            [['idGrupo', 'idUnidade'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idGrupo' => 'Id Grupo',
            'idUnidade' => 'Id Unidade',
        ];
    }
}
