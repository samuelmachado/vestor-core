<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "solicitacao".
 *
 * @property int $id Cód.
 * @property int $idCliente Cliente
 * @property int $idLocutor Locutor
 * @property string $descricao Descrição
 * @property string $anexo Anexo
 * @property string $dataHora Data
 * @property string $dataHoraEnvio Envio
 * @property string $dataHoraGravacao Gravação
 *
 * @property Usuario $cliente
 * @property Usuario $locutor
 */
class Solicitacao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'solicitacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCliente', 'idLocutor', 'descricao'], 'required'],
            [['idCliente', 'idLocutor'], 'integer'],
            [['dataHora', 'dataHoraEnvio', 'dataHoraGravacao','status'], 'safe'],
            [['descricao'], 'string', 'max' => 5000],
            //[['anexo'], 'string', 'max' => 120],
            [['idCliente'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idCliente' => 'id']],
            [['idLocutor'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idLocutor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Cód.',
            'idCliente' => 'Cliente',
            'idLocutor' => 'Locutor',
            'descricao' => 'Descrição',
            'anexo' => 'Anexo',
            'dataHora' => 'Data',
            'dataHoraEnvio' => 'Envio',
            'dataHoraGravacao' => 'Gravação',
            'status' =>'Status',
            'documento' => 'Anexos da solicitação'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idCliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocutor()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idLocutor']);
    }

    public function getDocumento()
    {
        return $this->hasMany(Documento::className(), ['idSolicitacao' => 'id']);
    }

}
