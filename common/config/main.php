<?php
return [
	'name' => 'Car sharing',
	'language' => 'pt-br',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'stmp.devell.com.br',
                'username' => 'thainan.prado@devell.com.br',
                'password' => 'DevellTap!@#123',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        

    ],
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module',
            // other module settings
        ]
    ]
];