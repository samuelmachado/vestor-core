<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Fabricante */

$this->title = 'Criar fabricante';
$this->params['breadcrumbs'][] = ['label' => 'Fabricantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabricante-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
