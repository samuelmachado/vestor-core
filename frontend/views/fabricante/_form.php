<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Fabricante */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fabricante-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <button type="submit" class="btn btn-success">Save</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
