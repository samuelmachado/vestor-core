<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Reserva */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reserva-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idUsuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'inicioReserva')->textInput() ?>

    <?= $form->field($model, 'fimReservsa')->textInput() ?>

    <?= $form->field($model, 'checkIn')->textInput() ?>

    <?= $form->field($model, 'checkOut')->textInput() ?>

    <?= $form->field($model, 'inicioKm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fimKm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idUnidadeOrigem')->textInput() ?>

    <?= $form->field($model, 'idUnidadeTermino')->textInput() ?>

    <?= $form->field($model, 'idUnidadeTerminoReal')->textInput() ?>

    <?= $form->field($model, 'cadastrado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
