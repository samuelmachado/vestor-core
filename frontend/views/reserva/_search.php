<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ReservaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reserva-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'idUsuario') ?>

    <?= $form->field($model, 'inicioReserva') ?>

    <?= $form->field($model, 'fimReservsa') ?>

    <?= $form->field($model, 'checkIn') ?>

    <?php // echo $form->field($model, 'checkOut') ?>

    <?php // echo $form->field($model, 'inicioKm') ?>

    <?php // echo $form->field($model, 'fimKm') ?>

    <?php // echo $form->field($model, 'idUnidadeOrigem') ?>

    <?php // echo $form->field($model, 'idUnidadeTermino') ?>

    <?php // echo $form->field($model, 'idUnidadeTerminoReal') ?>

    <?php // echo $form->field($model, 'cadastrado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
