<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use branchonline\lightbox\Lightbox;

// Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
/* @var $this yii\web\View */
/* @var $model common\models\Reserva */

$this->title = 'Reserva #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Reservas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
function format($source){
    $d = new DateTime($source);
    return $d->format('d/m/Y H:i');
}
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">

                <p>
                    <?= Html::a('Remover', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                        'confirm' => 'Deseja realmente excluir este item?',
                        'method' => 'post',
                        ],
                        ]) ?>
                    </p>

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                        'id',
                        [
                        'attribute' => 'idUsuario',
                        'label' => 'Usuário',
                        'value' => function ($data) {
                            return $data->usuario->nome;
                        },
                        ],
                        [
                        'attribute' => 'inicioReserva',
                        'label' => 'Início Reserva',
                        'value' => function ($data) {
                            $d = new DateTime($data->inicioReserva);
                            return $d->format('d/m/Y H:i');
                        },

                        ],
                        [
                        'attribute' => 'fimReserva',
                        'label' => 'Fim Reserva',
                        'value' => function ($data) {
                            $d = new DateTime($data->fimReserva);
                            return $d->format('d/m/Y H:i');
                        },

                        ],
                        [
                        'attribute' => 'checkIn',
                        'label' => 'Check In',
                        'value' => function ($data) {
                            $d = new DateTime($data->checkIn);
                            return $d->format('d/m/Y H:i');
                        },

                        ],
                        [
                        'attribute' => 'checkOut',
                        'label' => 'Check Out',
                        'value' => function ($data) {
                            $d = new DateTime($data->checkOut);
                            return $d->format('d/m/Y H:i');
                        },

                        ],



                        [
                        'attribute' => 'idUnidadeOrigem',
                        'label' => 'Unidade Origem',
                        'value' => function ($data) {
                            return $data->unidadeOrigem->nome;
                        },
                        ],
                        [
                        'attribute' => 'idUNidadeTermino',
                        'label' => 'Unidade Destino',
                        'value' => function ($data) {
                            return $data->unidadeTermino->nome;
                        },
                        ],
                        'cadastrado',
                        ],
                        ]) ?>

                    </div>
                </div>
            </div>
        <div class="col-md-6">
                <div class="box box-primary">
                <div class="box-header with-border">
                <h1>Ocorrências</h1>
       
    <?php
        foreach ($model->ocorrencias as $ocorrencia) {
            print '<div class="row col-md-12">';
            print '<h3>#'.$ocorrencia->id.' registrada em '.format($ocorrencia->cadastrado).'</h3>';
            print '<p>Descrição: '.$ocorrencia->descricao.'</p>';
         
            foreach($ocorrencia->documentos as $documento){
                print ' <div class="col-md-2 thumb" style="text-align: center;">';
                if (substr($documento->arquivo, -3) != 'pdf')
                    echo Lightbox::widget([
                        'files' => [
                            [
                                'thumb' => 'img/default.png',
                                'original' => $documento->arquivo,
                                'title' => 'Arquivo',
                                'target' => '_new'
                            ],
                        ]
                    ]);
                else
                    echo '<a href="'.$documento->arquivo.'" class="" target="_new"><img class="img-responsive" src="img/pdf.png"></a>';
        
            print '</div>';
            }
            print '</div>';
     
        }
     ?>
              </div>
     </div>
        </div>
        </div>
