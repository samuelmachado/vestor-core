<?php
use yii\helpers\Html;
use common\models\Usuario;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini"><img src="img/Logo Negativo.png"></span><span class="logo-lg"><img src="img/Logo Negativo.png"></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <i class="fa fa-bars"></i>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="img/profile-default-img.png" class="user-image" alt="User Image"/>
                        <span class="user-name hidden-xs"><?= Yii::$app->user->identity->nome; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="img/profile-default-img.png" class="img-circle"
                                 alt="User Image"/>
                            <p>
                                <?= Yii::$app->user->identity->nome; ?>
                                <small><?= Yii::$app->user->identity->meuPerfil; ?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a(
                                    'Perfil',
                                    ['/usuario/view', 'id' => Yii::$app->user->identity->id],
                                    ['data-method' => 'post', 'class' => 'btn btn-flat btn btn-flat-default btn btn-flat-flat']
                                ) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sair',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-flat btn btn-flat-default btn btn-flat-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <?php if (Yii::$app->user->identity->idPerfil == Usuario::PERFIL_SUPER_ADMIN) { ?>
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
</header>

<style>
.sidebar-toggle:before{
    content: ""!important;
}
</style>
