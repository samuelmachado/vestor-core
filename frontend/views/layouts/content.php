<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Usuario;
use common\models\Cliente;
use kartik\widgets\FileInput;

?>
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="content_title-principal">
            <?= \yii\helpers\Html::encode($this->title); ?>
        </h1>
        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Versão</b> Alfa
    </div>
    <span>Desenvolvido pela GEO SP</span>
</footer>

</aside><!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>

<script type="text/javascript">
    $( document ).ready(function() {
        $('#select-cliente-atual').change(function(){
            $.get("index.php?r=usuario%2Fset-client&id="+$(this).val(), function(data, status){
                document.location.reload(true);
            }, "json");
        })
    })
</script>
