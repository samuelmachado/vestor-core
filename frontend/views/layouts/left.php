<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
                 <div class="pull-left image">
                    <img src="img/profile-default-img.png" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>Olá <?= Yii::$app->user->identity->nome; ?> !</p>
                </div>
            </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    [
                        'label' => 'Dashboard',
                        'icon' => 'fas fa-columns',
                        'url' => ['dashboard/index'],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Cadastro',
                        'icon' => 'fas fa-pencil-alt',
                        'items' => [
                            ['label' => 'Usuários', 'icon' => 'fa fa-user', 'url' => ['usuario/index'],],
                            ['label' => 'Regional', 'icon' => 'fas fa-home', 'url' => ['regional/index'],],
                            ['label' => 'Grupo', 'icon' => 'fas fa-users', 'url' => ['grupo/index'],],
                            ['label' => 'Unidade', 'icon' => 'fas fa-object-ungroup', 'url' => ['unidade/index'],],
                            ['label' => 'Tipo veiculo', 'icon' => 'fas fa-truck-moving', 'url' => ['tipo-veiculo/index'],],
                            ['label' => 'Veiculo', 'icon' => 'fa fa-car', 'url' => ['veiculo/index'],],
                            ['label' => 'Postos credenciados', 'icon' => 'fa fa-user', 'url' => ['postos-credenciados/index'],],
                        ],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Relatório',
                        'icon' => 'fas fa-chart-bar',
                        'items' => [
                            ['label' => 'Reservas/Motoristas', 'icon' => 'fa fa-user', 'url' => ['reserva/index'],],
                            ['label' => 'Relatório veículo', 'icon' => 'fa fa-car', 'url' => ['veiculo/relatorio-veiculo'],],
                        ],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Sair',
                        'icon' => 'fa fa-arrow-right',
                        'url' => ['site/logout'],
                        'visible' => !Yii::$app->user->isGuest,
                        'template' => '<a href="{url}" data-method="post">{icon} {label}</a>',
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>

<script>
    for(var i=0; i < $('i').length; i++){
        $('i')[i].className = $('i')[i].className.replace('fa-fa','');
    }
</script>
