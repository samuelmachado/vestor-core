<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\Regional */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">
    <?php $form = ActiveForm::begin([
            'encodeErrorSummary' => false,
            'errorSummaryCssClass' => 'help-block']); ?>

        <?= $form->errorSummary($model) ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?=
                $form->field($model, 'descricao')->widget(TinyMce::className(), [
                    'options' => ['rows' => 6],
                    'language' => 'pt_BR',
                    'clientOptions' => [
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste"
                        ],
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ]
                ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
        <?= $form->field($model, 'imagem')->widget(FileInput::classname(), [
            'options' => ['accept' => '*', 'id' => 'imagem'],
            'pluginOptions' => [
                'language' => substr(\Yii::$app->language, 0, 2),
                'showPreview' => true,
                'showUpload' => false,
                'showRemove' => false,
                'showCancel' => false,
                // 'showPreview' => false,
                'showCaption' => false,
                'initialPreview' => $model->imagem == null ? [] : [$model->imagem],
                'initialPreviewAsData' => true,
                'msgPlaceholder' => '',
                'browseLabel' => 'Procurar',
                'browseIcon' => '',
                'dropZoneTitle' => 'Arraste a imagem aqui',
                'initialCaption' =>  $model->imagem == null ? [] : ['1 arquivo selecionados'],
            ],
        ])->label('Imagem') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <style>
        #mceu_14{
            z-index: 1031;
        }
        .krajee-default .file-footer-buttons .kv-file-remove{
            float: right;
            display: none;
        }
    </style>
</div>
