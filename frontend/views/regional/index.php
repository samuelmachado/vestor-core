<?php

use common\models\Regional;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RegionalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Regional';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">

            <div class="box-shadow-container">
              <div class="box-body">
                <div class="overflow-auto">
                  <?= Html::a('<i class="fa fa-plus" style="font-size:12px;padding:5px 5px;"></i> Regional', ['create'], ['class' => 'button-insert btn btn-success pull-right']) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [

                            'nome',
                            [
                                'format' => 'raw',
                                'attribute' => 'descricao'
                            ],
                            [

                                'label' => 'Quantidade de grupos',
                                'format' => 'html',
                                'value' => function ($model) {

                                    return Html::tag('span', count($model->unidades));
                                }

                            ],
                            [

                                'attribute' => 'Quantidade de unidades',
                                'label' => "Quantidade de unidades",
                                'format' => 'html',
                                'value' => function ($model) {

                                    return Html::tag('span', count($model->unidades));
                                }

                            ],
                            [
                                'class' =>  'yii\grid\ActionColumn',
                                'template' => '{view} {update} {delete}',
                                'buttons'  => [
                                  'delete' => function($url, $model) {
                                          return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model['id']], [
                                                  'title' => Yii::t('app', 'Remover'), 'data-confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),'data-method' => 'post']);
                                  }
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
