<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Usuario;

/* @var $this yii\web\View */

/* @var $form yii\widgets\ActiveForm */
?>

<aside class="main-sidebar">

    <section class="sidebar">

<?php print Html::a('<span class="logo-mini"><img src="img/icone.png" style="width:100%; margin-top: -60px;"></span><span class="logo-lg"><img src="img/logo.png" style="width:80%;margin:-10% 10% 10% 10%"></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
        <?php
            if (!Yii::$app->user->isGuest)
            {
        ?>
            <!-- Sidebar user panel -->
            <div class="user-panel">
                 <div class="pull-left image">
                    <img src="img/profile-default-img.png" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>Olá <?= Yii::$app->user->identity->nome; ?> !</p>
                </div>
            </div>
        <?php
            }
        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Veículos',
                        'icon' => 'fa fa-car',
                        'url' => ['veiculo/index'],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Reservas',
                        'icon' => 'fa fa-cogs',
                        'url' => ['reserva/index'],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                    // [
                    //     'label' => 'Ocorrências',
                    //     'icon' => 'fa fa-book',
                    //     'url' => ['ocorrencia/index'],
                    //     'visible'=>!Yii::$app->user->isGuest
                    // ],
                    [
                        'label' => 'Unidades',
                        'icon' => 'fa fa-building',
                        'url' => ['unidade/index'],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Usuários',
                        'icon' => 'fa fa-users',
                        'url' => ['usuario/index'],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Regional',
                        'icon' => 'fa fa-home',
                        'url' => ['regional/index'],
                        'visible'=>!Yii::$app->user->isGuest
                    ],                    [
                        'label' => 'Grupo',
                        'icon' => 'fa fa-users',
                        'url' => ['grupo/index'],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Tipo de licença',
                        'icon' => 'fa fa-list',
                        'url' => ['tipo-licenca/index'],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Sair',
                        'icon' => 'fa fa-arrow-right',
                        'url' => ['site/logout'],
                        'visible' => !Yii::$app->user->isGuest,
                        'template' => '<a href="{url}" data-method="post">{icon} {label}</a>',
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
