<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TipoLicenca */

$this->title = $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Tipo Licencas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">

                <p>
                    <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger float-right ml-1 border-radius-none',
                            'data' => [
                                'confirm' => 'Deseja realmente excluir este item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    <?= Html::a('Atualizar', ['update', 'id' => $model->id], 
                        [
                            'class' => 'btn btn-primary float-right ml-10 border-radius-none',
                        ]) ?>
                    
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'nome',
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>