<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TipoLicenca */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body">

    <?php $form = ActiveForm::begin([
            'encodeErrorSummary' => false,
            'errorSummaryCssClass' => 'help-block']); ?>

        <?= $form->errorSummary($model) ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-8">
        </div>
    </div>
    <div class="form-group">
    <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary pull-right']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>