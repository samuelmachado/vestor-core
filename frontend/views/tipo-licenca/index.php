<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TipoLicencaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tipo de licença';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">

            <div class="box-header with-border">
                <?= Html::a('<i class="fa fa-plus" style="font-size:12px;padding:5px 5px;"></i> Licença', ['create'], ['class' => 'btn btn-success pull-right']) ?>
            </div>
            <div class="box-body">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        // ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        'nome',
                        [

                            'attribute' => 'Quantidade de motoristas',
                            'label' => 'Quantidade de motoristas',
                            'format' => 'html',
                            'value' => function ($model) {

                                return Html::tag('span', count($model->motoristas));
                            }

                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}{delete}',
                            'buttons'  => [
                                'delete' => function($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model['id']], [
                                                'title' => Yii::t('app', 'Remover'), 'data-confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),'data-method' => 'post']);
                                }
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
