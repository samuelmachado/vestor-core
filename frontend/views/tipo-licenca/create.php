<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TipoLicenca */

$this->title = 'Cadastrar tipo de licença';
$this->params['breadcrumbs'][] = ['label' => 'Tipos de licenças', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-solid">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

		</div>
	</div>
</div>
