<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Grupo */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="dashboard">

      <div class="box-small">
        <div class="box-small-titles">
          <h2 class="sub-title">Reserva</h2>
          <p class="title-description">Visão geral das reservas</p>
        </div>
        <div id="reserva" class="graph-highchart"></div>
      </div>

      <div class="box-small">
        <div class="box-small-titles">
          <h2 class="sub-title">Reserva por regional</h2>
        </div>
        <div id="reserva-regional" class="graph-highchart"></div>
      </div>

      <div class="box-small">
        <div class="box-small-titles">
          <h2 class="sub-title">Reserva motivos</h2>
        </div>
        <div id="reserva-motivos" class="graph-highchart"></div>
      </div>


      <div class="box-small">
        <div class="box-small-titles">
          <h2 class="sub-title">Veículo</h2>
          <p class="title-description">Visão geral da frota</p>
        </div>
        <div id="veiculo" class="graph-highchart"></div>
      </div>

      <div class="box-medium">
        <div class="box-small-titles">
          <h2 class="sub-title">Avaliação</h2>
        </div>
        <div id="avaliacao" class="graph-highchart"></div>
      </div>
    </div>
</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script type="text/javascript">

Highcharts.chart('reserva', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: ''
  },
  legend: {
    // align: 'middle',
    // verticalAlign: 'bottom'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: false,
      cursor: 'pointer',
      dataLabels: {
        enabled: false
      },
      showInLegend: true
    }
  },
  series: [
    {
      innerSize: '60%',
      name: "",
      colorByPoint: true,
      data: [
        {
          name: "Realizada",
          y: 25,
          color: '#253658'
        },
        {
          name: "Em andamento",
          y: 25,
          color: '#4B69A6'
        },
        {
          name: "Agendada",
          y: 25,
          color: '#47A3B3'
        },
        {
          name: "Cancelada",
          y: 25,
          color: '#CACACA'
        }
      ]
    }
  ]
})
Highcharts.chart('reserva-regional', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: ''
  },
  legend: {
    align: 'middle',
    verticalAlign: 'bottom',
    layout: 'horizontal'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: false,
      cursor: 'pointer',
      dataLabels: {
        enabled: false
      },
      showInLegend: true
    }
  },
  series: [
    {
      innerSize: '60%',
      name: "",
      colorByPoint: true,
      data: [
        {
          name: "BA",
          y: 25,
          color: '#253658'
        },
        {
          name: "SP",
          y: 25,
          color: '#CACACA'
        },
        {
          name: "ES",
          y: 25,
          color: '#47A3B3'
        },
        {
          name: "RJ",
          y: 25,
          color: '#4B69A6'
        }
      ]
    }
  ]
})
Highcharts.chart('avaliacao', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  subtitle: {
    text: ''
  },
  legend: {
    align: 'right',
    verticalAlign: 'middle',
    layout: 'horizontal'
  },
  xAxis: {
    categories: [''],
    labels: {
      x: -10
    }
  },
  yAxis: {
    allowDecimals: false,
    title: {
        text: ''
    }
  },
  series: [{
    name: 'Nota 0',
    data: [0]
  },
  {
    name: 'Nota 1',
    data: [2]
  },
  {
    name: 'Nota 2',
    data: [3]
  },
  {
    name: 'Nota 3',
    data: [2]
  },
  {
    name: 'Nota 4',
    data: [6]
  },
  {
    name: 'Nota 5',
    data: [9]
  },
  {
    name: 'Nota 6',
    data: [15]
  },
  {
    name: 'Nota 7',
    data: [11]
  },
  {
    name: 'Nota 8',
    data: [17]
  },
  {
    name: 'Nota 9',
    data: [19]
  },
  {
    name: 'Nota 10',
    data: [16]
  }],
  responsive: {
    rules: [{
      condition: {
          maxWidth: 500
      },
      chartOptions: {
        legend: {
          align: 'center',
          verticalAlign: 'bottom',
          layout: 'horizontal'
        },
        yAxis: {
          labels: {
            align: 'left',
            x: 0,
            y: -5
          },
          title: {
            text: null
          }
        },
        subtitle: {
          text: null
        },
        credits: {
          enabled: false
        }
      }
    }]
  }
})
Highcharts.chart('reserva-motivos', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: ''
  },
  legend: {
    align: 'middle',
    verticalAlign: 'bottom',
    layout: 'horizontal'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: false,
      cursor: 'pointer',
      dataLabels: {
        enabled: false
      },
      showInLegend: true
    }
  },
  series: [{
    name: '',
    innerSize: '60%',
    colorByPoint: true,
    data: [{
      name: 'Viagem',
      y: 50,
      color: '#4B69A6',
      sliced: false
    }, {
      name: 'Rota',
      y: 25,
      color: '#47A3B3'
    }, {
      name: 'Rota crítica',
      y: 25,
      color: '#253658'
    }]
  }]
})
Highcharts.chart('veiculo', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: ''
  },
  legend: {
    align: 'middle',
    verticalAlign: 'bottom',
    layout: 'horizontal'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: false,
      cursor: 'pointer',
      dataLabels: {
        enabled: false
      },
      showInLegend: true
    }
  },
  series: [
    {
      innerSize: '60%',
      name: "",
      colorByPoint: true,
      data: [
        {
          name: "Estacionado",
          y: 75,
          color: '#4B69A6'
        },
        {
          name: "Em uso",
          y: 25,
          color: '#47A3B3'
        }
      ]
    }
  ]
})

</script>
