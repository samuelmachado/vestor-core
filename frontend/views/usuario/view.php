<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Usuario */

$this->title = $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Usuários', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-md-12">
        <div class="box-shadow-container">
          <div class="row">
            <div class="col-md-4 form-title-span">
              <label class="form-label-title">Nome</label>
              <span class="form-label-span"><?= $model->nome ?></span>
            </div>
            <div class="col-md-4 form-title-span">
              <label class="form-label-title">E-mail</label>
              <span class="form-label-span"><?= $model->email ?></span>
            </div>
            <div class="col-md-4 form-title-span">
              <label class="form-label-title">Status</label>
              <span class="form-label-span"><?= $model->idPerfil ?></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-title-span">
              <label class="form-label-title">Grupo</label>
              <span class="form-label-span"><?= $model->idPerfil ?></span>
            </div>
            <div class="col-md-4 form-title-span">
              <label class="form-label-title">Área</label>
              <span class="form-label-span"><?= $model->idPerfil ?></span>
            </div>
            <div class="col-md-4 form-title-span">
              <label class="form-label-title">Cargo</label>
              <span class="form-label-span"><?= $model->idTipoLicenca ?></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-title-span">
              <label class="form-label-title">Unidade</label>
              <span class="form-label-span"><?= $model->idTipoLicenca ?></span>
            </div>
            <div class="col-md-4 form-title-span">
              <label class="form-label-title">Imagem</label>
              <img class="form-label-img" src="<?= $model->imagem ?>">
            </div>
          </div>
        <p class="buttons-group">
          <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary float-right ml-10 border-radius-none']) ?>
          <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger float-right ml-10 border-radius-none',
            'data' => [
              'confirm' => 'Deseja realmente excluir este item?',
              'method' => 'post',
            ],
            ]) ?>
        </p>
      </div>
    </div>
</div>
