<?php

use common\models\TipoLicenca;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Usuario;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuários';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
          <div class="box-shadow-container">
            <div class="box-body">
              <div class="overflow-auto">
                <?= Html::a('<i class="fa fa-plus" style="font-size:12px;padding:5px 5px;"></i> Usuário', ['create'], ['class' => 'button-insert btn btn-success pull-right']) ?>
                <?php Pjax::begin(); ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]);
                ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        // ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        [
                            'label' => 'Área',
                            'attribute' => 'idPerfil',
                            'filter' => ['valor 1', 'valor 2', 'valor 3'],
                            'value' => function($data){
                              return null;
                            },
                        ],
                        'nome',
                        // 'email:email',
                        [
                            'label' => 'E-mail',
                            'attribute' => 'email',
                            // 'filter' => Usuario::ARRAY_STATUS,
                            'value' => function ($data) {
                                return $data['email'];
                            },
                        ],
                        [
                            'label' => 'Status',
                            'attribute' => 'status',
                            'filter' => Usuario::ARRAY_STATUS,
                            'value' => function ($data) {
                                return $data['meuStatus'];
                            },
                        ],
                        [
                            'label' => 'Grupo',
                            'attribute' => 'idTipoLicenca',
                            'filter' => ArrayHelper::map(TipoLicenca::find()->all(), 'id', 'nome'),
                            'value' => 'tipoLicenca.nome'
                        ],
                        [
                            'label' => 'Cargo',
                            'attribute' => 'idTipoLicenca',
                            'filter' => ArrayHelper::map(TipoLicenca::find()->all(), 'id', 'nome'),
                            'value' => 'tipoLicenca.nome'
                        ],
                        [
                            'label' => 'Unidade',
                            'attribute' => 'rotaCritica',
                            'filter' => Usuario::ARRAY_ROTAS,
                            'value' => function ($data) {
                                return $data['rotaCritica'] ? $data['minhaRota'] : '';
                            },
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {update} {delete}',
                            'buttons'  => [
                                'view'   => function ($url, $model) {
                                    return Html::a('<span class="fa fa-eye"></span>', $url, ['title' => 'Ver']);
                                },
                                'delete' => function($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model['id']], [
                                            'title' => Yii::t('app', 'Remover'), 'data-confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),'data-method' => 'post']);
                                }
                            ]
                        ],
                    ],
                ]); ?>
                </div>
                <?php Pjax::end(); ?>
            </div>
          </div>
        </div>
    </div>
</div>
<style media="screen">

</style>
