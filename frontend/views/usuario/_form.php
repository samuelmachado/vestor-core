<?php

use common\models\TipoLicenca;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Usuario;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">


    <?php $form = ActiveForm::begin([
        'encodeErrorSummary' => false,
        'errorSummaryCssClass' => 'help-block']); ?>

    <?= $form->errorSummary($model) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label('E-mail')  ?>
        </div>

        <div class="col-md-4">
          <?= $form->field($model, 'password')->textInput()->passwordInput()  ?>
        </div>
    </div>

    <div class="row">
      <div class="col-md-4">
        <?= $form->field($model, 'password')->textInput()->passwordInput()->label('Repita a senha')  ?>
      </div>
      <div class="col-md-4">
          <?= $form->field($model, 'status')->dropDownList([1 => 'ATIVO', 0 => 'INATIVO'],['prompt' => 'Selecionar status'])->label('Status') ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'idPerfil')->dropDownList(Usuario::ARRAY_PERFIS, ['prompt' => 'Selecionar perfil'])->label('Grupo') ?>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4">
        <?= $form->field($model, 'idPerfil')->dropDownList(Usuario::ARRAY_PERFIS, ['prompt' => 'Selecionar perfil'])->label('Área') ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'idPerfil')->dropDownList(Usuario::ARRAY_PERFIS, ['prompt' => 'Selecionar perfil'])->label('Cargo') ?>
      </div>
      <div class="col-md-4">
          <?= $form->field($model, 'idTipoLicenca')->dropDownList(ArrayHelper::map(TipoLicenca::find()->all(), 'id', 'nome'), ['prompt' => 'Selecionar regional'])->label('Unidade') ?>
      </div>
    </div>

    <div class="row">
        <div class="col-md-12">
        <?= $form->field($model, 'imagem')->widget(FileInput::classname(), [
            'options' => ['accept' => '*', 'id' => 'imagem'],
            'pluginOptions' => [
                'language' => substr(\Yii::$app->language, 0, 2),
                'showPreview' => true,
                'showUpload' => false,
                'showRemove' => false,
                'showCancel' => false,
                // 'showPreview' => false,
                'showCaption' => false,
                'initialPreview' => $model->imagem == null ? [] : [$model->imagem],
                'initialPreviewAsData' => true,
                'msgPlaceholder' => '',
                'browseLabel' => 'Procurar',
                'browseIcon' => '',
                'dropZoneTitle' => 'Arraste a imagem aqui',
                'initialCaption' =>  $model->imagem == null ? [] : ['1 arquivo selecionados'],
            ],
        ])->label('Imagem') ?>
        </div>
    </div>
<!--
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'chaveDallas')->textInput()  ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'rotaCritica')->dropDownList([1 => 'SIM', 0 => 'NÃO'],['prompt' => 'Selecionar rota crítica']) ?>
        </div>
    </div> -->

    <!-- <?php if($model->created_at) echo $form->field($model, 'updated_at')->textInput(['maxlength' => true, 'value'=>date('Y-m-d H:i:s'), 'type'=> 'hidden']) ?>

    <?php if(!$model->created_at) echo $form->field($model, 'created_at')->textInput(['maxlength' => true, 'value'=>date('Y-m-d H:i:s'), 'type'=> 'hidden']) ?> -->

    <div class="form-group buttons-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('Voltar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
