<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\Regional;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UnidadeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Unidade';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
          <div class="box-shadow-container">
            <div class="box-body">

                <div class="overflow-auto">
                  <?= Html::a('<i class="fa fa-plus" style="font-size:12px;padding:5px 5px;"></i>  Unidade', ['create'], ['class' => 'button-insert btn btn-success pull-right']) ?>
                  <?php Pjax::begin(); ?>
                  <?php // echo $this->render('_search', ['model' => $searchModel]);
                  ?>


                  <?= GridView::widget([
                      'dataProvider' => $dataProvider,
                      'filterModel' => $searchModel,
                      'columns' => [
                          // ['class' => 'yii\grid\SerialColumn'],
                          [
                              'label' => 'Nome da unidade',
                              'attribute' => 'idRegional'
                          ],
                          [
                              'attribute' => 'idRegional',
                              'filter' => ArrayHelper::map(Regional::find()->all(), 'id', 'nome'),
                              'value' => 'regional.nome'
                          ],
                          'endereco',

                          [
                              'class' => 'yii\grid\ActionColumn',
                              'template' => '{view}{update}{delete}',
                              'buttons'  => [
                                  'delete' => function($url, $model) {
                                          return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model['id']], [
                                                  'title' => Yii::t('app', 'Remover'), 'data-confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),'data-method' => 'post']);
                                  }
                              ],
                          ],
                      ],
                  ]); ?>
                  <?php Pjax::end(); ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
