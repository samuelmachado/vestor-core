<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Unidade */

$this->title = 'Editar unidade';
$this->params['breadcrumbs'][] = ['label' => 'Unidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="row">
    <div class="col-md-12">
        <div class="box-shadow-container">
          <div class="box box-solid">
              <?= $this->render('_form', [
                  'model' => $model,
              ]) ?>
          </div>
        </div>
    </div>
</div>
