<?php

use common\models\Regional;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Unidade */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="box-body">
  <?php $form = ActiveForm::begin([
        'encodeErrorSummary' => false,
        'errorSummaryCssClass' => 'help-block']); ?>

    <?= $form->errorSummary($model) ?>
  <div class="row">
    <div class="col-md-4">
      <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'idRegional')->dropDownList(ArrayHelper::map(Regional::find()->all(), 'id', 'nome'), ['prompt' => 'Selecionar regional']) ?>
    </div>
    <div class="col-md-4">
      <?= $form->field($model, 'endereco')->textInput(['maxlength' => true]) ?>
    </div>
  </div>
  <?= $form->field($model, 'lat')->hiddenInput(['maxlength' => true])->label(false) ?>
  <?= $form->field($model, 'lng')->hiddenInput(['maxlength' => true])->label(false) ?>

  <div class="form-group buttons-group">
    <?= Html::submitButton('Voltar', ['class' => 'btn btn-primary']) ?>
    <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
  window.onload = function() {

    $("#unidade-endereco").geocomplete()
      .bind("geocode:result", function(event, result) {
        console.log("Result: " + result.formatted_address);
        console.log(result)
        $("#unidade-lat").val(result.geometry.location.lat())
        $("#unidade-lng").val(result.geometry.location.lng())
      })
      .bind("geocode:error", function(event, status) {
        console.log("ERROR: " + status);
      })
      .bind("geocode:multiple", function(event, results) {
        console.log("Multiple: " + results.length + " results found");
      });

    // $(document).on("submit", "#formLocal", function (e) {
    //   e.preventDefault();
    //   e.stopImmediatePropagation();
    //   // Se exerce função especifica, tem que descrever
    //   if (!$( ".myCheck#aceite").hasClass('checked'))
    //   {
    //       $( ".myCheck#aceite").addClass('alert');
    //       return false;
    //   }
    // })

  };
</script>
