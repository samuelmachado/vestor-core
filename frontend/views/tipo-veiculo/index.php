<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Veiculo;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TipoVeiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tipo de veículo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">

            <div class="box-shadow-container">
              <div class="box-body">
                <div class="overflow-auto">
                  <?= Html::a('<i class="fa fa-plus" style="font-size:12px;padding:5px 5px;"></i> Tipo de veículo', ['create'], ['class' => 'button-insert btn btn-success pull-right']) ?>
                  <?= GridView::widget([
                      'dataProvider' => $dataProvider,
                      'filterModel' => $searchModel,
                      'columns' => [
                          // ['class' => 'yii\grid\SerialColumn'],

                          // 'id',
                          [
                              'label' => 'Modelo/Marca',
                              'attribute' => 'nome',
                              'filter' => ['valor 1', 'valor 2', 'valor 3'],
                              'value' => function($data){
                                return null;
                              },
                          ],
                          [

                              'attribute' => 'Quantidade de veículos',
                              'label' => 'Quantidade de veículos',
                              'format' => 'html',
                              'value' => function ($data) {
                                  return Html::tag('span', count($data->veiculos));
                              }

                          ],
                          [
                              'class' => 'yii\grid\ActionColumn',
                              'template' => '{view}{update}{delete}',
                              'buttons'  => [
                                  'delete' => function($url, $model) {
                                          return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model['id']], [
                                                  'title' => Yii::t('app', 'Remover'), 'data-confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),'data-method' => 'post']);
                                  }
                              ],
                          ],
                      ],
                  ]); ?>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
