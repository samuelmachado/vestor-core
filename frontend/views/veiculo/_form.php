<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\TipoVeiculo;
use common\models\Usuario;

/* @var $this yii\web\View */
/* @var $model common\models\Veiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin();
    ?>
    <div class="row">
    	<div class="col-md-4">
    		<?= $form->field($model, 'idTipoVeiculo')->dropDownList(ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),['prompt' => 'SELECIONE'] ) ?>
    	</div>
      <div class="col-md-4">
        <?= $form->field($model, 'placa')->dropDownList(ArrayHelper::map(Usuario::find()->all(), 'id', 'nome'),['prompt' => 'SELECIONE'] ) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'idDonoVeiculo')->dropDownList(ArrayHelper::map(Usuario::find()->all(), 'id', 'nome'),['prompt' => 'SELECIONE'] ) ?>
      </div>

    </div>
    <div class="row">
    	<div class="col-md-4">
        <div class="veiculo_titulo-horarios">Horários disponíveis</div>
        <div class="col-md-4 veiculo_horario-check">
          <label>
            <input type="checkbox" id="dia-todo">
            Dia todo <br> (24h)
          </label>
        </div>
        <div class="col-md-4">
          <label class="veiculo_label-horario">Inicio</label>
      		<?= $form->field($model, 'horarioInicio')->dropDownList(ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),['prompt' => 'SELECIONE'] ) ?>
      	</div>
        <div class="col-md-4">
          <label class="veiculo_label-horario">Fim</label>
          <?= $form->field($model, 'horarioFinal')->dropDownList(ArrayHelper::map(Usuario::find()->all(), 'id', 'nome'),['prompt' => 'SELECIONE'] ) ?>
        </div>
      </div>
      <div class="col-md-4">
          <?= $form->field($model, 'idUnidade')->dropDownList(ArrayHelper::map(Usuario::find()->all(), 'id', 'nome'),['prompt' => 'SELECIONE'] ) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'emUso')->dropDownList(ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),['prompt' => 'SELECIONE'] ) ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
    		<?= $form->field($model, 'semParar')->dropDownList(ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),['prompt' => 'SELECIONE'] ) ?>
    	</div>
    </div>

		<?php  if($model->created_at) echo $form->field($model, 'updated_at')->textInput(['maxlength' => true, 'value'=>date('Y-m-d H:i:s'), 'type'=> 'hidden']) ?>


		<?php if(!$model->created_at) echo $form->field($model, 'created_at')->textInput(['maxlength' => true, 'value'=>date('Y-m-d H:i:s'), 'type'=> 'hidden']) ?>

    <div class="form-group buttons-group">
      <?= Html::submitButton('Voltar', ['class' => 'btn btn-primary']) ?>
      <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>
label[for='veiculo-created_at']{
    display:none;
}
label[for='veiculo-updated_at']{
    display:none;
}
</style>
