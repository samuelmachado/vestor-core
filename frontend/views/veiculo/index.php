<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\TipoVeiculo;
use common\models\Usuario;
use common\models\Veiculo;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VeiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Veículo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
          </div>

          <div class="box-shadow-container">
            <div class="box-body">
              <div class="overflow-auto">
                <?= Html::a('<i class="fa fa-plus" style="font-size:12px;padding:5px 5px;"></i> Veículos', ['create'], ['class' => 'button-insert btn btn-success pull-right']) ?>
                <?php Pjax::begin(); ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        // ['class' => 'yii\grid\SerialColumn'],
                        // 'id',

                        [
                            'attribute' => 'idTipoVeiculo',
                            'value' => function ($data) {
                                return TipoVeiculo::findOne($data->idTipoVeiculo)['nome'];
                            },
                            'filter' => ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),
                        ],
                        [
                          'label' => 'Placa',
                          'attribute' => 'idTipoVeiculo',
                          'filter' => ['valor 1', 'valor 2', 'valor 3'],
                          'value' => function($data){
                            return null;
                          },
                        ],
                        [
                          'label' => 'Dias disponíveis',
                          'attribute' => 'idTipoVeiculo',
                          'filter' => ['valor 1', 'valor 2', 'valor 3'],
                          'value' => function($data){
                            return null;
                          },
                        ],
                        [
                          'label' => 'Horários disponiveis',
                          'attribute' => 'idTipoVeiculo',
                          'filter' => ['valor 1', 'valor 2', 'valor 3'],
                          'value' => function($data){
                            return null;
                          },
                        ],
                        [
                            'label' => 'Unidade',
                            'attribute' => 'idTipoVeiculo',
                            'filter' => ['valor 1', 'valor 2', 'valor 3'],
                            'value' => function($data){
                              return null;
                            },
                        ],
                        [
                            'label' => 'Em uso',
                            'attribute' => 'idTipoVeiculo',
                            'filter' => ['valor 1', 'valor 2', 'valor 3'],
                            'value' => function($data){
                              return null;
                            },
                        ],
                        [
                            'label' => 'Sem parar',
                            'attribute' => 'idTipoVeiculo',
                            'filter' => ['valor 1', 'valor 2', 'valor 3'],
                            'value' => function($data){
                              return null;
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{visualizar-relatorio-veiculo} {update} {delete}',
                            'buttons'  => [
                                'visualizar-relatorio-veiculo' => function ($url, $model) {
                                    return Html::a('<span class="fa fa-eye"></span>', $url, ['title' => 'Ver']);
                                },
                                'alert' => function ($url, $model) {
                                    return  Html::a('<span class="glyphicon glyphicon-alert"></span>');
                                }
                            ]
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
              </div>
            </div>
          </div>

        </div>
    </div>
</div>
