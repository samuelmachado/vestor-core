<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\TipoVeiculo;
use common\models\Usuario;
use common\models\Veiculo;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VeiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Relatório veículos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
          </div>

          <div class="box-shadow-container">
            <div class="box-body">
              <div class="overflow-auto">
                <?= Html::a('Limpar filtro', ['create'], ['class' => 'button-clear']) ?>
                <?php Pjax::begin(); ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        // ['class' => 'yii\grid\SerialColumn'],
                        // 'id',

                        [
                            'label' => 'Placa',
                            'attribute' => 'idTipoVeiculo',
                            'filter' => ['valor 1', 'valor 2', 'valor 3'],
                            'value' => function($data){
                              return null;
                            },
                        ],
                        [
                            'attribute' => 'idTipoVeiculo',
                            'value' => function ($data) {
                                return TipoVeiculo::findOne($data->idTipoVeiculo)['nome'];
                            },
                            'filter' => ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),
                        ],
                        [
                            'label' => 'Km',
                            'attribute' => 'idTipoVeiculo',
                            'filter' => ['valor 1', 'valor 2', 'valor 3'],
                            'value' => function($data){
                              return null;
                            },
                        ],
                        [
                            'label' => 'Status da reserva',
                            'attribute' => 'idTipoVeiculo',
                            'filter' => ['valor 1', 'valor 2', 'valor 3'],
                            'value' => function($data){
                              return null;
                            },
                        ],
                        [
                            'label' => 'Unidade',
                            'attribute' => 'idTipoVeiculo',
                            'filter' => ['valor 1', 'valor 2', 'valor 3'],
                            'value' => function($data){
                              return null;
                            },
                        ],
                        [
                          'label' => 'Avaria',
                          'attribute' => 'idDonoVeiculo',
                          'value' => function ($data) {
                            return Usuario::findOne($data->idDonoVeiculo)['nome'];
                          },
                          'filter' => ArrayHelper::map(Usuario::find()->all(), 'id', 'nome')
                        ],
                        [
                            'label' => 'Em uso',
                            'attribute' => 'idTipoVeiculo',
                            'filter' => ['valor 1', 'valor 2', 'valor 3'],
                            'value' => function($data){
                              return null;
                            },
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{alert} {visualizar-relatorio}',
                            'buttons'  => [
                                'visualizar-relatorio'   => function ($url, $model) {
                                    return Html::a('<span class="fa fa-eye"></span>', $url, ['title' => 'Ver']);
                                },
                                'alert' => function ($url, $model) {
                                    return  Html::a('<span class="glyphicon glyphicon-alert"></span>');
                                }
                            ]
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
              </div>
            </div>
          </div>

        </div>
    </div>
</div>
