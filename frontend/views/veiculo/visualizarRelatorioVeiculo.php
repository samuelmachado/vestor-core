<div class="row">
    <div class="col-md-12">
      <h1 class="content_title-principal">Relatório veículo</h1>
      <div class="box-shadow-container">
        <div class="row">
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Código da reserva</label>
            <span class="form-label-span"><?= $model->id ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Motorista</label>
            <span class="form-label-span"><?= $model->idUsuario ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Placa</label>
            <span class="form-label-span"><?= $model->id ?></span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Veículo</label>
            <span class="form-label-span"><?= $model->id ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Check in</label>
            <span class="form-label-span"><?= $model->checkIn ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Check out</label>
            <span class="form-label-span"><?= $model->checkOut ?></span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">CDD Origem</label>
            <span class="form-label-span"><?= $model->idUnidadeOrigem ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Motivo da reserva</label>
            <span class="form-label-span"><?= $model->id ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Avaria</label>
            <span class="form-label-span"><?= $model->id ?></span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 form-title-span">
            <label class="form-label-title">Imagem</label>
            img aqui
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 form-title-span">
            <label class="form-label-title">Observações</label>
            <span class="form-label-span"><?= $model->id ?></span>
          </div>
        </div>
        <p class="buttons-group">
          <button class="btn btn-primary" name="button">Voltar</button>
        </p>
    </div>
  </div>
</div>
