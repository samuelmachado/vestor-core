<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Veiculo */

$this->title = 'Veículo';
$this->params['breadcrumbs'][] = ['label' => 'Veiculos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-md-12">
      <div class="box-shadow-container">
        <div class="row">
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Tipo de veículo</label>
            <span class="form-label-span"><?= $model->idTipoVeiculo ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Placa</label>
            <span class="form-label-span"><?= $model->placa ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Data</label>
            <span class="form-label-span"><?= $model->idTipoVeiculo ?></span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Horários disponíveis</label>
            <span class="form-label-span"><?= $model->horarioInicio ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Unidade</label>
            <span class="form-label-span"><?= $model->idUnidade ?></span>
          </div>
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Em uso</label>
            <span class="form-label-span"><?= $model->emUso ?></span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 form-title-span">
            <label class="form-label-title">Sem parar</label>
            <span class="form-label-span"><?= $model->semParar ?></span>
          </div>
        </div>
        <p class="buttons-group">
          <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary float-right ml-10 border-radius-none']) ?>
          <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger float-right ml-10 border-radius-none',
            'data' => [
              'confirm' => 'Deseja realmente excluir este item?',
              'method' => 'post',
            ],
            ]) ?>
        </p>
    </div>
  </div>
</div>
