<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Veiculo */

$this->title = 'Editar veículo';
$this->params['breadcrumbs'][] = ['label' => 'Veiculos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
	<div class="col-md-12">
		<div class="box-shadow-container">
			<div class="box box-solid">

				<?= $this->render('_form', [
				'model' => $model,
				]) ?>
			</div>
		</div>
		</div>
	</div>
