<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\Reserva;


/* @var $this yii\web\View */
/* @var $model common\models\Veiculo */

$this->title = 'Relatório motorista';
$this->params['breadcrumbs'][] = ['label' => 'Motorista', 'url' => ['motorista']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
              <?php
                $gridColumns = [
                  [
                    'attribute' => 'nome',
                  ],
                  [
                    'attribute' => 'created_at',
                    'label'=> 'Últimos 3 meses',
                    'value' => function ($data) {
                      return Reserva::getQtd3Mouth($data->id);
                    },
                  ],
                  [
                    'attribute' => 'created_at',
                    'label'=> 'Últimos 6 meses',
                    'value' => function ($data) {
                      return Reserva::getQtd6Mouth($data->id);
                    },
                  ],
                  [
                    'attribute' => 'created_at',
                    'label'=> 'Últimos 12 meses',
                    'value' => function ($data) {
                      return Reserva::getQtd12Mouth($data->id);
                    },
                  ],
              ];

              ?>
              <?= ExportMenu::widget([
                  'dataProvider' => $dataProvider,
                  'filterModel' => $searchModel,
                  'columns' => $gridColumns,
                ]);
              ?>
          </div>

  <div class="box-header with-border">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
              'attribute' => 'nome',
            ],
            [
              'attribute' => 'created_at',
              'label'=> 'Últimos 3 meses',
              'value' => function ($data) {
                $dataNow = new DateTime();
                $dataNow->sub(new DateInterval('P3M'));
                $Mouth3Ago = date_format($dataNow, 'Y-m-d');
                return count(Reserva::find()->where(['idUsuario'=>$data->id])->andFilterWhere(['between', 'inicioReserva', $Mouth3Ago,date_format( new DateTime(),'Y-m-d')])->all());
              },
            ],
            [
              'attribute' => 'created_at',
              'label'=> 'Últimos 6 meses',
              'value' => function ($data) {
                $dataNow = new DateTime();
                $dataNow->sub(new DateInterval('P6M'));
                $Mouth3Ago = date_format($dataNow, 'Y-m-d');
                return count(Reserva::find()->where(['idUsuario'=>$data->id])->andFilterWhere(['between', 'inicioReserva', $Mouth3Ago,date_format( new DateTime(),'Y-m-d')])->all());
              },
            ],
            [
              'attribute' => 'created_at',
              'label'=> 'Últimos 12 meses',
              'value' => function ($data) {
                $dataNow = new DateTime();
                $dataNow->sub(new DateInterval('P12M'));
                $Mouth3Ago = date_format($dataNow, 'Y-m-d');
                return count(Reserva::find()->where(['idUsuario'=>$data->id])->andFilterWhere(['between', 'inicioReserva', $Mouth3Ago,date_format( new DateTime(),'Y-m-d')])->all());
              },
            ],
        ],
    ]); ?>

      </div>
		</div>
	</div>
</div>
