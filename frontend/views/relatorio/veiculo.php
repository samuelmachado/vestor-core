<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;
use common\models\TipoVeiculo;
use common\models\Usuario;
use common\models\Veiculo;
use common\models\Reserva;

/* @var $this yii\web\View */
/* @var $model common\models\Veiculo */

$this->title = 'Relatório veículo';
$this->params['breadcrumbs'][] = ['label' => 'Veículo', 'url' => ['veiculo']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
              <?php
                $gridColumns = [
                  [
                    'attribute' => 'idTipoVeiculo',
                    'label' => 'Carro em uso',
                    'value' => function ($data) {
                        return TipoVeiculo::findOne($data->idTipoVeiculo)['nome'];
                    },
                    'filter' => ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),
                  ],
                  [
                    'attribute' => 'emUso',
                    'label'=> 'Em uso',
                    'value' => function ($data) {
                          if(Veiculo::findOne($data->id)['emUso'] == '1') return 'Sim';
                          else return 'Não';
                    },
                    'filter' => ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),
                  ],
                  [
                    'attribute' => 'created_at',
                    'label'=> 'Últimos 3 meses',
                    'value' => function ($data) {
                      return Reserva::getQtd3Mouth($data->idDonoVeiculo);
                    },
                  ],
                  [
                    'attribute' => 'veiculoPlaca',
                    'label'=> 'Placa',
                    'value' => function ($data) {
                      return Reserva::getQtd3Mouth($data->veiculoPlaca);
                    },
                  ],
                  [
                    'attribute' => 'diasDisponiveis',
                    'label'=> 'Dias disponíveis',
                    'value' => function ($data) {
                      return Reserva::getQtd3Mouth($data->diasDisponiveis);
                    },
                  ],
                  [
                    'attribute' => 'horariosDisponiveis',
                    'label'=> 'Horários disponíveis',
                    'value' => function ($data) {
                      return Reserva::getQtd3Mouth($data->horariosDisponiveis);
                    },
                  ],
                  [
                    'attribute' => 'horariosDisponiveis',
                    'label'=> 'Horários disponíveis',
                    'value' => function ($data) {
                      return Reserva::getQtd3Mouth($data->idDonoVeiculo);
                    },
                  ],
                  [
                    'attribute' => 'created_at',
                    'label'=> 'Últimos 6 meses',
                    'value' => function ($data) {
                      return Reserva::getQtd6Mouth($data->idDonoVeiculo);
                    },
                  ],
                  [
                    'attribute' => 'created_at',
                    'label'=> 'Últimos 12 meses',
                    'value' => function ($data) {
                      return Reserva::getQtd12Mouth($data->idDonoVeiculo);
                    },
                  ],
              ];

              ?>
              <?= ExportMenu::widget([
                  'dataProvider' => $dataProvider,
                  'filterModel' => $searchModel,
                  'columns' => $gridColumns,
                ]);
              ?>
          </div>

  <div class="box-header with-border">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
          [
            'attribute' => 'idTipoVeiculo',
            'label' => 'Carro em uso',
            'value' => function ($data) {
                return TipoVeiculo::findOne($data->idTipoVeiculo)['nome'];
            },
            'filter' => ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),
          ],
          [
            'attribute' => 'emUso',
            'label'=> 'Em uso',
            'value' => function ($data) {
                  if(Veiculo::findOne($data->id)['emUso'] == '1') return 'Sim';
                  else return 'Não';
            },
            'filter' => ArrayHelper::map(TipoVeiculo::find()->all(), 'id', 'nome'),
          ],
          [
            'attribute' => 'created_at',
            'label'=> 'Últimos 3 meses',
            'value' => function ($data) {
              return Reserva::getQtd3Mouth($data->idDonoVeiculo);
            },
          ],
          [
            'attribute' => 'created_at',
            'label'=> 'Últimos 6 meses',
            'value' => function ($data) {
              return Reserva::getQtd6Mouth($data->idDonoVeiculo);
            },
          ],
          [
            'attribute' => 'created_at',
            'label'=> 'Últimos 12 meses',
            'value' => function ($data) {
              return Reserva::getQtd12Mouth($data->idDonoVeiculo);
            },
          ],
        ],
    ]); ?>

      </div>
		</div>
	</div>
</div>
