<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Grupo */

$this->title = 'Cadastrar Grupo';
$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box-shadow-container">
          <div class="box box-solid">
              <?= $this->render('_form', [
                  'model' => $model,
              ]) ?>
          </div>
        </div>
    </div>
</div>
