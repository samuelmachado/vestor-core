<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Unidade;
/* @var $this yii\web\View */
/* @var $model common\models\Grupo */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body">

<?php $form = ActiveForm::begin([
    'encodeErrorSummary' => false,
    'errorSummaryCssClass' => 'help-block',
]); ?>
<?= $form->errorSummary($model) ?>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'descricao')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-4">
      <?php foreach ($model->unidades as $unidade) $model->inputUnidades[] = $unidade->id; ?>
      <?=
           $form->field($model, 'inputUnidades')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(Unidade::find()->all(), 'id', 'nome'),
                  'value' => $model->inputUnidades,
                  'language' => 'pt',
                  'options' => [
                          'placeholder' => 'Selecione todas as unidades',
                          'class' => 'form-control',
                          'id' => 'gruposGrupo',
                          'multiple' => true,
                  ],
                  'pluginOptions' => [
                      'tags' => true,
                      'tokenSeparators' => [',', ' '],
                      'allowClear' => true,
                      'multiple' => true,
                      'initialize' => true,
                  ],
              ]);
      ?>
    </div>
</div>

<div class="form-group buttons-group">
    <?= Html::submitButton('Voltar', ['class' => 'btn btn-primary']) ?>
    <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
