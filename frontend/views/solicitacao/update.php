<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Solicitacao */

$this->title = 'Editar solicitação: #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Solicitacaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="solicitacao-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
