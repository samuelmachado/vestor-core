<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Solicitacao */

$this->title = 'Solicitação #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Solicitacaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="solicitacao-view">

    <p>
        <?= Html::a('Atualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=  \Yii::$app->User->identity->idPerfil == 1 ? Html::a('Apagar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Deseja realmente excluir este item?',
                'method' => 'post',
            ],
        ]) : '';?>
        <?= $model->anexo ? Html::a('Baixar Áudio', ['download', 'id' => $model->id], ['class' => 'btn btn-info']) : ''; ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'idCliente',
                'label' => 'Cliente',
                'value' => function ($data) {
                    return $data->cliente->nome;
                }
            ],
            [
                'attribute' => 'idLocutor',
                'label' => 'Locutor',
                'value' => function ($data) {
                    return $data->locutor->nome;
                }
            ],
            'descricao',
            'anexo',

            [
                'attribute' => 'dataHora',
                'label' => 'Registrado',
                'value' => function ($data) {
                     return $data->dataHoraEnvio ? date('d/m/Y H:i',strtotime($data->dataHora)) : '';
                }
            ],
            [
                'attribute' => 'dataHoraEnvio',
                'label' => 'Data do Envio',
                'value' => function ($data) {
                     return $data->dataHoraEnvio ? date('d/m/Y H:i',strtotime($data->dataHoraEnvio)) : '';
                }
            ],
            [
                'attribute' => 'dataHoraGravacao',
                'label' => 'Data da Gravação',
                'value' => function ($data) {
                     return $data->dataHoraGravacao ? date('d/m/Y H:i',strtotime($data->dataHoraGravacao)) : '';
                }
            ]
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box-body">
                <div class=" margin-bottom">
                    <?php  if ($model->documento) { 
                        foreach ($model->documento as $documento)
                        {
                            if (substr($documento->arquivo, -3) != 'pdf')
                                echo '<a href="'.$documento->arquivo.'" target="_blank" class="lightbox col-md-1"><img class="img-responsive" src="img/default.png"></a>';
                            else
                                echo '<a href="'.$documento->arquivo.'" class="col-md-1" target="_blank"><img class="img-responsive" src="img/pdf.png"></a>';
                        }
                    } else {
                        echo '<h4 class="vazio">SEM ANEXOS</h4>';
                    } ?>
                </div>
            </div>
        </div>
    </div>

</div>
