<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Solicitacao */

$this->title = 'Criar solicitação';
$this->params['breadcrumbs'][] = ['label' => 'Solicitações', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitacao-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
