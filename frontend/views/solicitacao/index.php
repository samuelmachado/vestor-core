<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Usuario;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SolicitacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitações';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitacao-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= \Yii::$app->User->identity->idPerfil == Usuario::PERFIL_SUPER_ADMIN ? Html::a('Criar Solicitação', ['create'], ['class' => 'btn btn-success']) : ''?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
  

            'id',
            [
                'attribute' => 'idCliente',
                'label' => 'Cliente',
                'value' => function ($data) {
                    return $data->cliente->nome;
                }
            ],
            [
                'attribute' => 'idLocutor',
                'label' => 'Locutor',
                'value' => function ($data) {
                    return $data->locutor->nome;
                }
            ],
          
         

            [
                'attribute' => 'dataHora',
                'label' => 'Registrado',
                'value' => function ($data) {
                     return $data->dataHoraEnvio ? date('d/m/Y H:i',strtotime($data->dataHora)) : '';
                }
            ],
            [
                'attribute' => 'dataHoraEnvio',
                'label' => 'Data do Envio',
                'value' => function ($data) {
                     return $data->dataHoraEnvio ? date('d/m/Y H:i',strtotime($data->dataHoraEnvio)) : '';
                }
            ],
            [
                'attribute' => 'dataHoraGravacao',
                'label' => 'Data da Gravação',
                'value' => function ($data) {
                     return $data->dataHoraGravacao ? date('d/m/Y H:i',strtotime($data->dataHoraGravacao)) : '';
                }
            ],
            //'dataHora',
            //'dataHoraEnvio',
            //'dataHoraGravacao',

            [
               'class' => 'yii\grid\ActionColumn',
               'template' => '{download} {view} {update} {delete}',
               'buttons' => [
                 'download' => function ($url, $model) {
                    return $model->anexo ? Html::a('<span class="glyphicon glyphicon-music"></span>', $url, [
                            'title' => Yii::t('app', 'Baixar áudio'),
                    ]) : '';
                },
                'delete' => function($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model['id']], [
                            'title' => Yii::t('app', 'Remover'), 'data-confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),'data-method' => 'post']);
                }
            ]
        ]
    ]
]);

?>
<?php Pjax::end(); ?>
</div>
