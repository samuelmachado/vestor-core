<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Usuario;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Solicitacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitacao-form">

    <?php $form = ActiveForm::begin([
            'encodeErrorSummary' => false,
            'errorSummaryCssClass' => 'help-block']); ?>

        <?= $form->errorSummary($model) ?>
     <?php if( \Yii::$app->User->identity->idPerfil == Usuario::PERFIL_SUPER_ADMIN || \Yii::$app->User->identity->idPerfil == Usuario::PERFIL_CLIENTE){ ?>  
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'idCliente')->dropDownList(ArrayHelper::map(Usuario::clientes(), 'id', 'nome' ), ['prompt' => 'SELECIONE']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'idLocutor')->dropDownList(ArrayHelper::map(Usuario::locutores(), 'id', 'nome' ), ['prompt' => 'SELECIONE']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
              <?= $form->field($model, 'descricao')->textarea(['rows' => '6']) ?>
        </div>
    
    </div>
    <div class="row">    
        <?php 
        $allimage = array();
        $configs = [];
        $docs = [];
        foreach ($model->documento as $index => $file) {
            $doc_url = Url::base('http').'/'.$file['arquivo'];
            $docs[] = $doc_url;
            if(substr($file['arquivo'], -3) == 'pdf'){
                $configs[] = ['caption' => $file['arquivo'], 'url' => 'index.php?r=solicitacao/deletar&id='.$file['id'], 'type' => 'pdf'];
            } else {
                $configs[] = ['caption' => $file['arquivo'], 'url' => 'index.php?r=solicitacao/deletar&id='.$file['id']];
            }
        }
        ?>
        <div class="col-md-12">
                      
              <?= $form->field($model, 'documento[]', [
                ])->widget(FileInput::classname(), [
                    'options' => [
                        'multiple' => true, 'accept' => 'image/*,application/pdf', 'id' => 'documentos'
                    ],
                    'pluginOptions' => [
                        'previewFileType' => 'image,pdf',
                        'showPreview' => true,
                        'showCaption' => true,
                        'showRemove' => false,
                        'initialPreviewAsData' => true,
                        'showUpload' => false,
                        'overwriteInitial' => false,
                        'browseClass' => 'btn btn-danger',
                        'initialPreview' => $docs,
                        'initialPreviewConfig' => $configs,
                        'language' => Yii::$app->language,
                    ],
                ]);
            ?> 
        </div>
    </div>
    <?php } ?>
    <div class="row">    
        <div class="col-md-12">
        <?php if(!$model->isNewRecord) {?>
             <?php echo $form->field($model, 'anexo')->widget(FileInput::classname(), [
                    'options'=>['accept'=>'audio/*', 'multiple'=>false],
                    'pluginOptions'=>[
                        'showPreview' => false,
                        'showCaption' => true,
                        'showRemove' => false,
                        'showUpload' => false,
                        'initialPreview' => $model->anexo,
                        'language' => Yii::$app->language,
                    ]
                ])->label('Áudio'); ?>
        <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
