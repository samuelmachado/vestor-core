<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SolicitacaoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitacao-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'idCliente') ?>

    <?= $form->field($model, 'idLocutor') ?>

    <?= $form->field($model, 'descricao') ?>

    <?= $form->field($model, 'anexo') ?>

    <?php // echo $form->field($model, 'dataHora') ?>

    <?php // echo $form->field($model, 'dataHoraEnvio') ?>

    <?php // echo $form->field($model, 'dataHoraGravacao') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
