<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper; 
use common\models\Fabricante;
use common\models\Veiculo;
use common\models\Usuario;  
use common\models\Reserva;  

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ocorrencia-index"> 
    <?php Pjax::begin(); ?>

    <?= GridView::widget([ 
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
             [
                'attribute' => 'idUsuario',
                'label' => 'Usuário',
                'value' => function ($data) {
                    return $data->usuario->nome;
                },
                'filter' => ArrayHelper::map(Usuario::find()->all(), 'id', 'nome')
            ],
            'descricao:ntext',
            [
                'attribute' => 'idReserva',
                'label' => 'Reserva',
                'value' => function ($data) {
                    return $data->id;
                },
                'filter' => ArrayHelper::map(Reserva::find()->all(), 'id', 'id')
            ],
            [
                'attribute' => 'idVeiculo',
                'label' => 'Veículo',
                'value' => function ($data) {
                    return $data->veiculo->placa;
                },
                'filter' => ArrayHelper::map(Veiculo::find()->all(), 'id', 'placa')
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
