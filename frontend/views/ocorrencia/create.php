<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Ocorrencia */

$this->title = 'Cadastrar ocorrência';
$this->params['breadcrumbs'][] = ['label' => 'Ocorrencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ocorrencia-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
