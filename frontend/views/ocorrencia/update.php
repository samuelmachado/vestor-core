<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ocorrencia */

$this->title = 'Update ocorrencia: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ocorrencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ocorrencia-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
