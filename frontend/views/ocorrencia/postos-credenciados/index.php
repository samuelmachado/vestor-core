<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PostosCredenciadosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Postos Credenciados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="postos-credenciados-index">


    <div class="box-shadow-container">
      <div class="box-body">
        <div class="overflow-auto">
          <?= Html::a('<i class="fa fa-plus" style="font-size:12px;padding:5px 5px;"></i> Posto', ['create'], ['class' => 'button-insert btn btn-success pull-right']) ?>
          <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

          <?= GridView::widget([
              'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
              'columns' => [
                  // ['class' => 'yii\grid\SerialColumn'],

                  // 'id',
                  'cnpj',
                  'nomeFantasia',
                  'lat',
                  // 'long',
                  //'endereco',
                  'idUnidade',
                  'idBandeira',
                  [
                      'class' => 'yii\grid\ActionColumn',
                      'template' => '{view} {update} {delete}'
                  ],
              ],
          ]); ?>
        </div>
      </div>
    </div>


</div>
