<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PostosCredenciados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="postos-credenciados-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
      <div class="col-md-4">
        <?= $form->field($model, 'cnpj')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'nomeFantasia')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'endereco')->textInput(['maxlength' => true]) ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <?= $form->field($model, 'idUnidade')->textInput() ?>
      </div>
    </div>

    <div class="form-group buttons-group">
      <?= Html::submitButton('Voltar', ['class' => 'btn btn-primary']) ?>
      <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
