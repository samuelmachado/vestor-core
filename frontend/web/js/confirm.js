yii.confirm = function (message = '', okCallback, cancelCallback) {
  swal({
          text: 'Deseja realmente excluir este item?',
          icon: 'warning',
          buttons : {
              cancel : {
                  text : "Oops! No",
                  value : null,
                  visible : true,
                  className : "",
                  closeModal : true
              },
              confirm : {
                  text : "Delete It Already",
                  value : true,
                  visible : true,
                  className : "",
                  closeModal : true
              }
          },
          closeOnClickOutside: true
  }).then((selection) => {
   if(selection){okCallback;}else{cancelCallback;}
  });
}