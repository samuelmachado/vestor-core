<?php

namespace frontend\controllers;

use Yii;
use common\models\TipoVeiculo;
use common\models\TipoVeiculoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TipoVeiculoController implements the CRUD actions for TipoVeiculo model.
 */
class TipoVeiculoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TipoVeiculo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TipoVeiculoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 20];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TipoVeiculo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TipoVeiculo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $dirBase = Yii::getAlias('@webroot') . '/';
        $dir = 'img/tipoveiculos/';
        $model = new TipoVeiculo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $imagem = UploadedFile::getInstance($model, 'imagem');

            if (!empty($imagem) && $imagem->extension != 'png' && $imagem->extension != 'jpg') {
                \Yii::$app->getSession()->setFlash('danger', 'O formato do arquivo deve ser jpg ou png');
                return $this->redirect(['create']);
            }

            if (!empty($imagem) && $imagem->size > 2000000) {
                \Yii::$app->getSession()->setFlash('danger', 'O tamanho máximo da imagem deve ser 2MB');
                return $this->redirect(['create']);
            }

            if (!file_exists($dirBase . $dir))
                mkdir($dir, 0777, true);

            if ($model->save()) {
                if (!empty($imagem)) {
                    $model->imagem = $dir . $model->id . "." . $imagem->extension;
                    $imagem->saveAs($model->imagem);
                    $model->save();
                }

                \Yii::$app->getSession()->setFlash('success', 'Tipo de veículo cadastrada com sucesso');
                return $this->redirect(['index']);
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TipoVeiculo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $dirBase = Yii::getAlias('@webroot') . '/';
        $dir = 'img/tipoveiculos/';
        $model = new TipoVeiculo();

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $imagem = UploadedFile::getInstance($model, 'imagem');

            if (!empty($imagem) && $imagem->extension != 'png' && $imagem->extension != 'jpg') {
                \Yii::$app->getSession()->setFlash('danger', 'O formato do arquivo deve ser jpg ou png');
                return $this->redirect(['create']);
            }

            if (!empty($imagem) && $imagem->size > 2000000) {
                \Yii::$app->getSession()->setFlash('danger', 'O tamanho máximo da imagem deve ser 2MB');
                return $this->redirect(['create']);
            }

            if (!file_exists($dirBase . $dir))
                mkdir($dir, 0777, true);

            if (!empty($imagem)) {

                $model->imagem = $dir . $model->id . "." . $imagem->extension;
                $imagem->saveAs($model->imagem);
                $model->save();
            } else {
                $model->imagem = $img;
                $model->save();
            }

            \Yii::$app->getSession()->setFlash('success', 'Tipo de veículo alterada com sucesso');

            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TipoVeiculo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TipoVeiculo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TipoVeiculo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TipoVeiculo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
