<?php

namespace frontend\controllers;

use Yii;
use common\models\Reserva;
use common\models\VeiculoSearch;
use common\models\UsuarioSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// use dosamigos\tableexport\ButtonTableExport;

/**
 * RelatorioController para gerar os relatórios.
 */
class RelatorioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lista os relatórios dos motoristas.
     * @return mixed
     */
    public function actionMotorista()
    {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 20];

        return $this->render('motorista', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Lista os relatórios dos veículos.
     * @return mixed
     */
    public function actionVeiculo()
    {

      $searchModel = new VeiculoSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('veiculo', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
