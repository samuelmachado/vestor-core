<?php

namespace frontend\controllers;

use Yii;
use common\models\Regional;
use common\models\RegionalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * RegionalController implements the CRUD actions for Regional model.
 */
class RegionalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Regional models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RegionalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 20];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Regional model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Regional model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $dirBase = Yii::getAlias('@webroot') . '/';
        $dir = 'img/unidades/';
        $model = new Regional();

        if ($model->load(Yii::$app->request->post())) {
            $imagem = UploadedFile::getInstance($model, 'imagem');

            if (!empty($imagem) && $imagem->extension != 'png' && $imagem->extension != 'jpg') {
                \Yii::$app->getSession()->setFlash('danger', 'O formato do arquivo deve ser jpg ou png');
                return $this->redirect(['create']);
            }

            if (!empty($imagem) && $imagem->size > 2000000) {
                \Yii::$app->getSession()->setFlash('danger', 'O tamanho máximo da imagem deve ser 2MB');
                return $this->redirect(['create']);
            }

            if (!file_exists($dirBase . $dir))
                mkdir($dir, 0777, true);

            if ($model->save()) {
                if (!empty($imagem)) {
                    $model->imagem = $dir . $model->id . "." . $imagem->extension;
                    $imagem->saveAs($model->imagem);
                    $model->save();
                }

                \Yii::$app->getSession()->setFlash('success', 'Regional cadastrada com sucesso');
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Regional model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $dirBase = Yii::getAlias('@webroot') . '/';
        $dir = 'img/unidades/';
        $model = $this->findModel($id);
        $img = $model->imagem;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $imagem = UploadedFile::getInstance($model, 'imagem');

            if (!empty($imagem) && $imagem->extension != 'png' && $imagem->extension != 'jpg') {
                \Yii::$app->getSession()->setFlash('danger', 'O formato do arquivo deve ser jpg ou png');
                return $this->redirect(['create']);
            }

            if (!empty($imagem) && $imagem->size > 2000000) {
                \Yii::$app->getSession()->setFlash('danger', 'O tamanho máximo da imagem deve ser 2MB');
                return $this->redirect(['create']);
            }

            if (!file_exists($dirBase . $dir))
                mkdir($dir, 0777, true);

            if (!empty($imagem)) {

                $model->imagem = $dir . $model->id . "." . $imagem->extension;
                $imagem->saveAs($model->imagem);
                $model->save();
            } else {
                $model->imagem = $img;
                $model->save();
            }

            \Yii::$app->getSession()->setFlash('success', 'Regional alterada com sucesso');
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Regional model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Regional model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Regional the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Regional::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
